<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>
    <div class="g-wrapper">
        <main>
            <div class="g-main">
                <div class="g-main_i container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class=" mt_2">
                                <h1>Корзина</h1>
                            </div>
                            <div class="basket_tabs">
                                <!-- Nav tabs -->
                                <ul class="basket_tabs__navigation" role="tablist">
                                    <li role="presentation" class="active"><a href="#basket" aria-controls="basket" role="tab" data-toggle="tab">Корзина</a></li>
                                    <li role="presentation"><a href="#" class="disable_a" aria-controls="delivery" role="tab" data-toggle="tab">Доставка</a></li>
                                    <li role="presentation"><a href="#" class="disable_a" aria-controls="order" role="tab" data-toggle="tab">Заказ</a></li>
                                </ul>
                                <div class="tab-content">
                                    <?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket", 
	"basket", 
	array(
		"ACTION_VARIABLE" => "basketAction",
		"AUTO_CALCULATION" => "Y",
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "DELETE",
			2 => "DELAY",
			3 => "PRICE",
			4 => "QUANTITY",
			5 => "PROPERTY_DESCRIPTION",
		),
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_CONVERT_CURRENCY" => "N",
		"GIFTS_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_PLACE" => "BOTTOM",
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "N",
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
		"HIDE_COUPON" => "Y",
		"OFFERS_PROPS" => array(
		),
		"PATH_TO_ORDER" => "/personal/order.php",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"TEMPLATE_THEME" => "blue",
		"USE_GIFTS" => "N",
		"USE_PREPAYMENT" => "N",
		"COMPONENT_TEMPLATE" => "basket"
	),
	false
);?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>