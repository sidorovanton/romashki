<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");

?><?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "breadcrumb",
    Array(
        "COMPONENT_TEMPLATE" => "breadcrumb",
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0"
    )
);

?> <main>
    <div class="g-main">
        <div class="g-main_i container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <ul class=" private_tabs__navigation mt_2" role="tablist">
                        <li role="presentation" class="active"><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">Мои заказы</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Мои настройки</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="orders">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:sale.personal.order.list",
                                "orders",
                                Array(
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "3600",
                                    "CACHE_TYPE" => "A",
                                    "COMPONENT_TEMPLATE" => "orders",
                                    "FILTER_HISTORY" => "Y",
                                    "HISTORIC_STATUSES" => array(''),
                                    "ID" => $ID,
                                    "NAV_TEMPLATE" => "",
                                    "ORDERS_PER_PAGE" => "20",
                                    "PATH_TO_BASKET" => "/personal/cart/",
                                    "PATH_TO_CANCEL" => "/personal/order/cancel/#ID#/",
                                    "PATH_TO_COPY" => "",
                                    "PATH_TO_DETAIL" => "/personal/order/detail/#ID#/",
                                    "PATH_TO_PAYMENT" => "payment.php",
                                    "SAVE_IN_SESSION" => "Y",
                                    "SET_TITLE" => "Y",
                                    "STATUS_COLOR_F" => "gray",
                                    "STATUS_COLOR_N" => "green",
                                    "STATUS_COLOR_P" => "yellow",
                                    "STATUS_COLOR_PSEUDO_CANCELLED" => "red"
                                )
                            );?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.profile",
                                "user.profile",
                                Array(
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "CHECK_RIGHTS" => "Y",
                                    "SEND_INFO" => "Y",
                                    "SET_TITLE" => "Y",
                                    "USER_PROPERTY" => "",
                                    "USER_PROPERTY_NAME" => ""
                                )
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>