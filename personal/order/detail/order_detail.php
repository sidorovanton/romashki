
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отмена заказа");

?><?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "breadcrumb",
    Array(
        "COMPONENT_TEMPLATE" => "breadcrumb",
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0"
    )
);
//$APPLICATION->AddChainItem('Мой кабинет');
?> <main>
    <div class="g-main">
        <div class="g-main_i container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <ul class=" private_tabs__navigation mt_2" role="tablist">
                        <li role="presentation" class="active"><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">Мои заказы</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Мои настройки</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="orders">
                            <?$APPLICATION->IncludeComponent("bitrix:sale.personal.order.detail", "order.detail", Array(
	"PATH_TO_LIST" => "/personal/order/",	// Страница со списком заказов
		"PATH_TO_CANCEL" => "/personal/order/cancel/#ID#/",	// Страница отмены заказа
		"PATH_TO_PAYMENT" => "payment.php",	// Страница подключения платежной системы
		"PATH_TO_COPY" => "",
		"ID" => $ID,	// Идентификатор заказа
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"PICTURE_WIDTH" => "110",	// Ограничение по ширине для анонсного изображения, px
		"PICTURE_HEIGHT" => "110",	// Ограничение по высоте для анонсного изображения, px
		"PICTURE_RESAMPLE_TYPE" => "1",	// Тип масштабирования
		"CUSTOM_SELECT_PROPS" => "",	// Дополнительные свойства инфоблока
		"PROP_1" => "",	// Не показывать свойства для типа плательщика "Физическое лицо" (s1)
		"PROP_2" => "",	// Не показывать свойства для типа плательщика "Юридическое лицо" (s1)
	),
	false
);?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.profile",
                            "user.profile",
                            Array(
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "CHECK_RIGHTS" => "Y",
                                "SEND_INFO" => "Y",
                                "SET_TITLE" => "Y",
                                "USER_PROPERTY" => "",
                                "USER_PROPERTY_NAME" => ""
                            )
                        );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
