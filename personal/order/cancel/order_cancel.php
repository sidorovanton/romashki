
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отмена заказа");

?><?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "breadcrumb",
    Array(
        "COMPONENT_TEMPLATE" => "breadcrumb",
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0"
    )
);

?> <main>
    <div class="g-main">
        <div class="g-main_i container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <ul class=" private_tabs__navigation mt_2" role="tablist">
                        <li role="presentation" class="active"><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">Мои заказы</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Мои настройки</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="orders">
                        <?$APPLICATION->IncludeComponent("bitrix:sale.personal.order.cancel","",Array(
                                "PATH_TO_LIST" => "/personal/order/",
                                "PATH_TO_DETAIL" => "/personal/order/detail/#ID#/",
                                "ID" => $ID,
                                "SET_TITLE" => "Y"
                            )
                        );?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.profile",
                            "user.profile",
                            Array(
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "CHECK_RIGHTS" => "Y",
                                "SEND_INFO" => "Y",
                                "SET_TITLE" => "Y",
                                "USER_PROPERTY" => "",
                                "USER_PROPERTY_NAME" => ""
                            )
                        );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
