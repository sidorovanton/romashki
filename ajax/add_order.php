<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER;
use Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Main\Application,
    Bitrix\Sale\DiscountCouponsManager;

if (!Loader::IncludeModule('sale'))
    die();

function getPropertyByCode($propertyCollection, $code)  {
    foreach ($propertyCollection as $property)
    {
        if($property->getField('CODE') == $code)
            return $property;
    }
}

$products = array(
    array('PRODUCT_ID' => $_POST['nIdProd'],
        'NAME' => $_POST['sProdName'],
        'PRICE' => $_POST['nPriceProd'],
        'CURRENCY' => $_POST['sCurrens'],
        'QUANTITY' => $_POST['nQuentityProd'])
);
$USER_NAME = $_POST['user_name'];
$USER_PHONE = $_POST['user_phone'];
$USER_ADDRES = $_POST['user_addres'];
$SITE_ID = $_POST['sSiteID'];

$basket = Bitrix\Sale\Basket::create($SITE_ID);

foreach ($products as $product)
{
    $item = $basket->createItem("catalog", $product["PRODUCT_ID"]);
    unset($product["PRODUCT_ID"]);
    $item->setFields($product);
}
$registeredUserID = $USER->GetID();
$order = Bitrix\Sale\Order::create($SITE_ID, (empty($registeredUserID) ? (\CSaleUser::GetAnonymousUserID()) : $registeredUserID));
$order->setPersonTypeId(1);
$order->setField('USER_DESCRIPTION', 'Заказ оставлен через кнопку "Заказать в 1 клик"');
$order->setField('STATUS_ID','OC');
$order->setBasket($basket);

$shipmentCollection = $order->getShipmentCollection();
$shipment = $shipmentCollection->createItem(
    Bitrix\Sale\Delivery\Services\Manager::getObjectById(1)
);

$shipmentItemCollection = $shipment->getShipmentItemCollection();

/** @var Sale\BasketItem $basketItem */

foreach ($basket as $basketItem)
{
    $item = $shipmentItemCollection->createItem($basketItem);
    $item->setQuantity($basketItem->getQuantity());
}

$paymentCollection = $order->getPaymentCollection();
$payment = $paymentCollection->createItem(
    Bitrix\Sale\PaySystem\Manager::getObjectById(1)
);
$payment->setField("SUM", $order->getPrice());
$payment->setField("CURRENCY", $order->getCurrency());

$order->doFinalAction(true);
$propertyCollection = $order->getPropertyCollection();
$nameProperty = getPropertyByCode($propertyCollection, 'FIO');
$nameProperty->setValue($USER_NAME);
$phoneProperty = getPropertyByCode($propertyCollection, 'PHONE');
$phoneProperty->setValue($USER_PHONE);
$addresProperty = getPropertyByCode($propertyCollection, 'ADDRESS');
$addresProperty->setValue($USER_ADDRES);
$result = $order->save();

if (!$result->isSuccess()){
    echo "Ошибка оформления";
}
else{
    echo "Ваш заказ оформлен";
}