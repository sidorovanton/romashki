<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница контактов");
?><div class="g-wrapper">
 <main> <section class="g-main">
	<div class="g-main_i ">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="text_center mb_2">Контакты</h1>
					<div class="col-xs-12 col-sm-4 mb_1">
						<div class="Lora-BoldItalic mb_1">
                            <span>АДРЕС МАГАЗИНА / Пункт самовывоза</span>
						</div>
						<ul class="list_reset">
							 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/include/adress_list.php"
	)
);?>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-8">
						<div class="row">
							<div class="col-xs-12 col-sm-4 mb_1">
								<div class="Lora-BoldItalic mb_1">
									 Телефоны:
								</div>
								<ul class="list_reset">
									 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/include/contacts_numbers.php"
	)
);?>
								</ul>
							</div>
							<div class="col-xs-12 col-sm-4 mb_1">
								<div class="Lora-BoldItalic mb_1">
									 Телефоны:
								</div>
								<ul class="list_reset">
									 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/include/contacts_emails.php"
	)
);?>
								</ul>
							</div>
							<div class="col-xs-12 col-sm-4 mb_1">
								<div class="Lora-BoldItalic mb_1">
									 Время работы:
								</div>
								<ul class="list_reset">
									 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/local/include/contacts_work_time.php"
	)
);?>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xs-12 mb_2">
						 <?$APPLICATION->IncludeComponent(
	"bitrix:map.google.view",
	"googlemap",
	Array(
		"API_KEY" => "AIzaSyBSSPDAo97Rum1h8w_ArKVtvLHzr6HdNis",
		"COMPONENT_TEMPLATE" => "googlemap",
		"CONTROLS" => array(0=>"SMALL_ZOOM_CONTROL",1=>"TYPECONTROL",2=>"SCALELINE",),
		"INIT_MAP_TYPE" => "ROADMAP",
		"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:53.93266663228654;s:10:\"google_lon\";d:27.555295505142457;s:12:\"google_scale\";i:17;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:18:\"Орловская\";s:3:\"LON\";d:27.55510032177;s:3:\"LAT\";d:53.932673875586;}}}",
		"MAP_HEIGHT" => "400",
		"MAP_ID" => "gm_1",
		"MAP_WIDTH" => "1000",
		"OPTIONS" => array(0=>"ENABLE_SCROLL_ZOOM",1=>"ENABLE_DBLCLICK_ZOOM",2=>"ENABLE_DRAGGING",3=>"ENABLE_KEYBOARD",)
	)
);?>
					</div>
					 <?$APPLICATION->IncludeComponent(
	"bitrix:feedback",
	"contacts.feedback",
	Array(
		"EMAIL_TO" => "ken.bitrix@gmail.com",
		"EVENT_MESSAGE_ID" => array(0=>"80",),
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"REQUIRED_FIELDS" => array(0=>"NAME",1=>"EMAIL",2=>"MESSAGE",),
		"USE_CAPTCHA" => "N",
	)
);?>
				</div>
			</div>
		</div>
	</div>
 </section> </main>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>