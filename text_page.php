<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Текстовая страница");
?>

    <div class="g-wrapper">
        <main>
            <section class="g-main">
                <div class="g-main_i ">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h1><?=$APPLICATION->GetTitle();?></h1>

                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => "/local/include/text_page_content.php"
                                    )
                                );?>
                                </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>