$(document).ready(function(){

    $('body').on('click', 'a#show_popap_order_back', function(){
        ProdPriceMain = $("div.card_price__current").text();
        ProdPriceMain = ProdPriceMain.substr(0, ProdPriceMain.length -5);
        ProdPriceMain = ProdPriceMain.replace(/\s+/g, '');
        printPrice = $("div.card_price__current").text();
        quentityProd = $("input.counter__inp").val();
        $('#popup-click input[name="nIdProd"]').val(ProdIdMain);
        $('#popup-click input[name="nPriceProd"]').val(+ProdPriceMain / +quentityProd);
        $('#popup-click input[name="sProdName"]').val(PropNameMain);
        $('#popup-click input[name="nQuentityProd"]').val(quentityProd);
        $("#popup-click .custom_text_pop").html('<div class="mb_1"><img src="'+ImageUrl+'" alt="'+PropNameMain+'"></div><div class="card_preview__lnk">'+PropNameMain+'</div><div class="card_preview__price mb_2">'+printPrice+'</div>');
        popupBay = $("#popup-click");
        $.magnificPopup.open({
            items: {
                src: popupBay,
                type: 'inline'
            }
        });
        $('body').on('click', '.close-button', function () {
            $.magnificPopup.close();
        });
    });

    $('body').on('submit', '#feed_order_user', function(){
        var arFormField = $(this).serializeArray();
        nCountFields = 10; //для успешной операции должны придти все 10 полей из формы
        if(arFormField.length >= nCountFields){
            $.ajax({
                type: 'POST',
                url: '/ajax/add_order.php',
                data: arFormField,
                success: function(nIdOrder){
                    if(nIdOrder){
                        $.magnificPopup.close();
                        popupBay = $("#card_in_basket");
                        $("#add_order_one_click").append(nIdOrder);
                        $('#card_in_basket .title_line_decor').text($("#add_order_one_click").text());
                        $('#card_in_basket #link_show').hide();
                        $.magnificPopup.open({
                            items: {
                                src: popupBay,
                                type: 'inline'
                            }
                        });
                        $('body').on('click', '.close-button', function () {
                            $.magnificPopup.close();
                        });
                    }
                }
            });
        }
        return false;
    });
});

function add2basketdalay(p_id,  pp_id, p, name, dpu){

    $.ajax({
        type: 'POST',
        url: '/wishajax.php',
        data: "p_id=" + p_id + "&pp_id=" + pp_id + "&p=" + p + "&name=" + name + "&dpu=" + dpu,
        success: function(dataMessage){
            console.log(dataMessage);
            $.trim(dataMessage);
            $('.wish span').text('('+dataMessage+')');
        }
    });
    return false;
};

$('body').on('click', 'a#add_delay_detail', function(){
    add2basketdalay(p_id,  pp_id, p, name, dpu);
    $("#in_wish .custom_popap").html('<div class="mb_1"><img src="'+ImageUrl+'" alt="img"></div><div class="card_preview__lnk mb_2">'+PropNameMain+'</div>');
    popupBay = $("#in_wish");
    $.magnificPopup.open({
        items: {
            src: popupBay,
            type: 'inline'
        }
    });

    $('body').on('click', '.close-button', function () {
        $.magnificPopup.close();
    });
});

function addProdDelay(nIdProd, nPriceProd, sCurrency, sLid, sNameProd, sLinkUrl, sUrlImg) {
    var sHTMLContent = "";
    var sThisPop = $("#in_wish");
    if (!sCurrency) {
        sCurrency = "BYR";
    }

    if (nIdProd && sCurrency && sLid && sNameProd) {
        add2basketdalay(nIdProd, nPriceProd, sCurrency, sLid, sNameProd);
    }
    if (sUrlImg) {
        sHTMLContent += '<div class="mb_1"><img src="' + sUrlImg + '" alt="' + sNameProd + '"></div>';
    }
    if (sLid) {
        sHTMLContent += '<div class="card_preview__lnk mb_2">' + sLid + '</div>';
    }
    $("#in_wish div.custom_popap").html(sHTMLContent);
    $.magnificPopup.open({
        items: {
            src: sThisPop,
            type: 'inline'
        }
    });
    $('body').on('click', '.close-button', function () {
        $.magnificPopup.close();
    });
   }

$(function($){
    $("#phone").mask("+375 (99) 999-99-99");
});








// masked_input_1.4-min.js
// angelwatt.com/coding/masked_input.php

