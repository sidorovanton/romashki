<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
    'LIST' => array(
        'CONT' => 'bx_sitemap',
        'TITLE' => 'bx_sitemap_title',
        'LIST' => 'bx_sitemap_ul',
    ),
    'LINE' => array(
        'CONT' => 'bx_catalog_line',
        'TITLE' => 'bx_catalog_line_category_title',
        'LIST' => 'bx_catalog_line_ul',
        'EMPTY_IMG' => $this->GetFolder() . '/images/line-empty.png'
    ),
    'TEXT' => array(
        'CONT' => 'bx_catalog_text',
        'TITLE' => 'bx_catalog_text_category_title',
        'LIST' => 'bx_catalog_text_ul'
    ),
    'TILE' => array(
        'CONT' => 'bx_catalog_tile',
        'TITLE' => 'bx_catalog_tile_category_title',
        'LIST' => 'bx_catalog_tile_ul',
        'EMPTY_IMG' => $this->GetFolder() . '/images/tile-empty.png'
    )
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>


<div class="col-md-9 col-sm-12">
    <div class="col-xs-12">
        <h1 class="mt_1">Каталог</h1>
    </div>
    <section class="row__inline-blocks mt_3">


        <?
        if (0 < $arResult["SECTIONS_COUNT"]) {
            ?>

            <?

            foreach ($arResult['SECTIONS'] as &$arSection) {
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                if (false === $arSection['PICTURE'])
                    $arSection['PICTURE'] = array(
                        'SRC' => $arCurView['EMPTY_IMG'],
                        'ALT' => (
                        '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                            ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                            : $arSection["NAME"]
                        ),
                        'TITLE' => (
                        '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                            ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                            : $arSection["NAME"]
                        )
                    );
                ?>
                <div class="col-xs-12 col-mid-xs-6 col-sm-4 ">
                    <div class="card_preview">

                        <a class="card_preview__lnk_pic" href="<? echo $arSection['SECTION_PAGE_URL']; ?>">
                            <img class="card_preview__pic" src="<? echo $arSection['PICTURE']['SRC']; ?>"
                                 title="<? echo $arSection['PICTURE']['TITLE']; ?>"
                                 alt="<? echo $arSection['PICTURE']['TITLE']; ?>">
                        </a>
                        <div class="card_preview__info">
                            <div class="card_preview__prod_name">
                                <a class="card_preview__lnk lnk_brd" href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
                                   title="<? echo $arSection['PICTURE']['TITLE']; ?>"><? echo $arSection['NAME']; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?
            }
            unset($arSection);


            ?>

            <?

            echo('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
        }
        ?>
    </section>
</div>
