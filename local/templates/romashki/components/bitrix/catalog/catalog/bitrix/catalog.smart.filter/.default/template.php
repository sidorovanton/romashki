<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder() . '/themes/' . $arParams['TEMPLATE_THEME'] . '/colors.css',
    'TEMPLATE_CLASS' => 'bx-' . $arParams['TEMPLATE_THEME']
);

if (isset($templateData['TEMPLATE_THEME'])) {

}

?>
<div class="filter_title mb_2">Подбор параметров</div>

<form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get"
      class="smartfilter">
    <? foreach ($arResult["HIDDEN"] as $arItem): ?>
        <input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>"
               value="<? echo $arItem["HTML_VALUE"] ?>"/>
    <? endforeach; ?>

    <div class="col-xs-12 col-sm-4">


        <div class="lnk_brd filter_lnk_collapse">
            Розничная цена
        </div>


        <div class="row">
            <? foreach ($arResult["ITEMS"] as $key => $arItem)//prices
            {
                $key = $arItem["ENCODED_ID"];
                if (isset($arItem["PRICE"])):
                    if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                        continue;

                    $precision = 2;
                    if (Bitrix\Main\Loader::includeModule("currency")) {
                        $res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
                        $precision = $res['DECIMALS'];
                    }
                    ?>

                    <div class="col-xs-6">
                        <div class="form-group count_price">
                            <label for="ot"><?= GetMessage("CT_BCSF_FILTER_FROM") ?></label>
                            <input
                                    class="form-control min-price"
                                    type="text"
                                    name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                    id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?> ot"
                                    value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                                    size="5"
                                    onkeyup="smartFilter.keyup(this)"
                            />
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group count_price">
                            <label for="do"><?= GetMessage("CT_BCSF_FILTER_TO") ?></label>

                            <input
                                    class="max-price form-control"
                                    type="text"
                                    name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                    id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?> do"
                                    value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                                    size="5"
                                    onkeyup="smartFilter.keyup(this)"
                            />

                        </div>
                    </div>


                <?
                $arJsParams = array(
                    "leftSlider" => 'left_slider_' . $key,
                    "rightSlider" => 'right_slider_' . $key,
                    "tracker" => "drag_tracker_" . $key,
                    "trackerWrap" => "drag_track_" . $key,
                    "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                    "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                    "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                    "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                    "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                    "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                    "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
                    "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                    "precision" => $precision,
                    "colorUnavailableActive" => 'colorUnavailableActive_' . $key,
                    "colorAvailableActive" => 'colorAvailableActive_' . $key,
                    "colorAvailableInactive" => 'colorAvailableInactive_' . $key,
                );
                ?>
                    <script type="text/javascript">
                        BX.ready(function () {
                            window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                        });
                    </script>
                <?endif;
            }
            ?>

        </div>
        <div class="clear"></div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="lnk_brd filter_lnk_collapse"> Какой повод
        </div>
        <?
        //not prices

        foreach ($arResult["ITEMS"] as $key => $arItem) {
            if (
                empty($arItem["VALUES"])
                || isset($arItem["PRICE"])
            )
                continue;

            if (
                $arItem["DISPLAY_TYPE"] == "A"
                && (
                    $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                )
            )
                continue;
            ?>

            <?
            $arCur = current($arItem["VALUES"]);
            switch ($arItem["DISPLAY_TYPE"]) {
            case "A"://NUMBERS_WITH_SLIDER
                ?>
                <?
                $arJsParams = array(
                    "leftSlider" => 'left_slider_' . $key,
                    "rightSlider" => 'right_slider_' . $key,
                    "tracker" => "drag_tracker_" . $key,
                    "trackerWrap" => "drag_track_" . $key,
                    "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                    "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                    "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                    "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                    "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                    "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                    "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
                    "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                    "precision" => $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0,
                    "colorUnavailableActive" => 'colorUnavailableActive_' . $key,
                    "colorAvailableActive" => 'colorAvailableActive_' . $key,
                    "colorAvailableInactive" => 'colorAvailableInactive_' . $key,
                );
                ?>
                <script type="text/javascript">
                    BX.ready(function () {
                        window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                    });
                </script>
            <?
            break;
            case "B"://NUMBERS
                ?>

                <?
                break;
            case "G"://CHECKBOXES_WITH_PICTURES
                ?>

                <?
                break;
            case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
                ?>

                <?
                break;
            case "P"://DROPDOWN
            $checkedItemExist = false;
            ?>
                <div class="bx-filter-select-container">

                    <div class="bx-filter-select-arrow"></div>
                    <input
                            style="display: none"
                            type="radio"
                            name="<?= $arCur["CONTROL_NAME_ALT"] ?>"
                            id="<? echo "all_" . $arCur["CONTROL_ID"] ?>"
                            value=""
                    />
                    <? foreach ($arItem["VALUES"] as $val => $ar): ?>
                        <input
                                style="display: none"
                                type="radio"
                                name="<?= $ar["CONTROL_NAME_ALT"] ?>"
                                id="<?= $ar["CONTROL_ID"] ?>"
                                value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                            <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                        />
                    <? endforeach ?>
                    <div class="bx-filter-select-popup" data-role="dropdownContent">
                        <div class="option_style">
                            <?
                            ?>
                            <select id="selectFilter_option" onchange="smartFilter.clickA(this,'selectFilter_option');"
                                    name="arrFilter_59" id="select_59">
                                <option class="<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>"
                                        for="<?= "all_" . $arCur["CONTROL_ID"] ?>">
                                    <? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
                                </option>
                                <?

                                foreach ($arItem["VALUES"] as $val => $ar):
                                    $class = "";
                                    if ($ar["CHECKED"])
                                        $class .= " selected";
                                    if ($ar["DISABLED"])
                                        $class .= " disabled";
                                    ?>
                                    <option
                                        <? foreach ($arItem["VALUES"] as $va => $arr) {
                                            if ($arr["CHECKED"] && $arr['VALUE'] == $ar['VALUE']) {
                                                echo 'selected';
                                                $checkedItemExist = true;
                                            }
                                        }
                                        ?> class="<?= $ar["VALUE"] ?>"><?= $ar["VALUE"] ?>
                                    </option>
                                <? endforeach ?>
                            </select>
                        </div>

                        <ul style="display:none">
                            <li>
                                <label class="<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>"
                                       for="<?= "all_" . $arCur["CONTROL_ID"] ?>" class="bx-filter-param-label"
                                       data-role="label_<?= "all_" . $arCur["CONTROL_ID"] ?>"
                                       onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape("all_" . $arCur["CONTROL_ID"]) ?>')">
                                    <? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
                                </label>
                            </li>
                            <?
                            foreach ($arItem["VALUES"] as $val => $ar):
                                $class = "";
                                if ($ar["CHECKED"])
                                    $class .= " selected";
                                if ($ar["DISABLED"])
                                    $class .= " disabled";
                                ?>
                                <li>
                                    <label class="<?= $ar["VALUE"] ?>" for="<?= $ar["CONTROL_ID"] ?>"
                                           class="bx-filter-param-label<?= $class ?>"
                                           data-role="label_<?= $ar["CONTROL_ID"] ?>"
                                           onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')"><?= $ar["VALUE"] ?></label>
                                </li>
                            <? endforeach ?>
                        </ul>
                    </div>

                </div>
                <?
                break;
                case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
                    ?>

                    <?
                    break;
                case "K"://RADIO_BUTTONS
                    ?>


                    <?
                    break;
                case "U"://CALENDAR
                    ?>
                    <?
                    break;
                default://CHECKBOXES
                    ?>
                    <?
            }
            ?>


            <?
        }
        ?>

    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="mt_2">


            <input
                    class="btn btn-primary"
                    type="submit"
                    id="set_filter"
                    name="set_filter"
                    value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
            />
            <input
                    class="btn btn-primary btn_reverse"
                    type="submit"
                    id="del_filter"
                    name="del_filter"
                    value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
            />
            <div class="bx-filter-popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"] ?>"
                 id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?>
                 style="display: inline-block;">
                <? echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">' . intval($arResult["ELEMENT_COUNT"]) . '</span>')); ?>
                <span class="arrow"></span>
                <br/>
                <a href="<? echo $arResult["FILTER_URL"] ?>" target=""></a>
            </div>
        </div>
    </div>


    <div class="clear"></div>
</form>

<script>
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>