<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
CJSCore::Init(array('clipboard'));

$APPLICATION->SetTitle("");

if (!empty($arResult['ERRORS']['FATAL'])) {
    foreach ($arResult['ERRORS']['FATAL'] as $error) {
        ShowError($error);
    }

    $component = $this->__component;

    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
        $APPLICATION->AuthForm('', false, false, 'N', false);
    }
} else {
    if (!empty($arResult['ERRORS']['NONFATAL'])) {
        foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
            ShowError($error);
        }
    }
    ?>

        <div class="bx_my_order_switch">
            <a class="bx_mo_link sale-order-detail-back-to-list-link-up"
               href="<?= htmlspecialcharsbx($arResult["URL_TO_LIST"]) ?>">
                &larr; <?= Loc::getMessage('SPOD_RETURN_LIST_ORDERS') ?>
            </a>
        </div>

        <div class="bx_order_list">
            <table class="bx_order_list_table">
                <thead>
                <tr>
                    <td colspan="2">
                        <?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
                            "#ACCOUNT_NUMBER#" => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
                            "#DATE_ORDER_CREATE#" => $arResult["DATE_INSERT_FORMATED"]
                        )) ?>
                    </td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <?= Loc::getMessage('SPOD_LIST_CURRENT_STATUS', array(
                            '#DATE_ORDER_CREATE#' => $arResult["DATE_INSERT_FORMATED"]
                        )) ?>
                    </td>
                    <td>
                        <?
                        if ($arResult['CANCELED'] !== 'Y') {
                            echo $arResult["STATUS"]["NAME"];
                            echo Loc::getMessage('SPOD_SUB_ORDER_OT', array(
                                '#DATE_ORDER_CREATE#' => $arResult["DATE_INSERT_FORMATED"]
                            ));
                        } else {
                            echo Loc::getMessage('SPOD_ORDER_CANCELED');
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?= Loc::getMessage('SPOD_ORDER_PRICE') ?>:
                    </td>
                    <td>
                        <?= $arResult["PRICE_FORMATED"] ?>
                    </td>
                </tr>
                <tr>
                    <td><?= Loc::getMessage('SPOD_ORDER_CANCELEDD') ?>:</td>
                    <td>
                        <?
                        if ($arResult['CANCELED'] !== 'Y') {
                            echo Loc::getMessage('SPOD_NO');
                            echo "  ";
                        } else {
                            echo Loc::getMessage('SPOD_YES') . "  ";
                        }
                        ?>
                        [<a href="/personal/order/cancel/<?= $arResult['ID'] ?>/?CANCEL=Y">Отменить</a>]
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td colspan="2"><?= Loc::getMessage('SPOD_TPL_ACCOUNT_INFO', array(
                            '#DATE_ORDER_CREATE#' => $arResult["DATE_INSERT_FORMATED"])) ?></td>
                </tr>
                <tr>
                    <td> <?= Loc::getMessage('SPOD_LOGIN') ?>:</td>
                    <td><? if (strlen($arResult["USER"]["EMAIL"])) {
                            ?>
                            <?= htmlspecialcharsbx($arResult["USER"]["LOGIN"]) ?>
                            <?
                        } ?>
                    </td>
                </tr>
                <tr>
                    <td><?= Loc::getMessage('SPOD_EMAIL') ?>:</td>
                    <td>

                        <a href="mailto:<?= htmlspecialcharsbx($arResult["USER"]["EMAIL"]) ?>"><?= htmlspecialcharsbx($arResult["USER"]["EMAIL"]) ?></a>


                </tr>

                <tr>
                    <td><br></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2"><?= Loc::getMessage('SPOD_ORDER_PROPERTIES') ?></td>
                </tr>
                <tr>
                    <td> <?= Loc::getMessage('SPOD_ORDER_PERS_TYPE') ?>:</td>
                    <td> <?= htmlspecialcharsbx($arResult["PERSON_TYPE"]["NAME"]) ?></td>
                </tr>
                <tr>
                    <td><br></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2"><?= Loc::getMessage('SPOD_TPL_ACCOUNT_PERSONAL_INFO') ?></td>
                </tr>

                <? if (isset($arResult["ORDER_PROPS"])) {
                    foreach ($arResult["ORDER_PROPS"] as $property) {
                        ?>
                        <tr>
                            <td><?= htmlspecialcharsbx($property['NAME']) ?>:</td>
                            <td>
                                <?
                                if ($property["TYPE"] == "Y/N") {
                                    echo Loc::getMessage('SPOD_' . ($property["VALUE"] == "Y" ? 'YES' : 'NO'));
                                } else {
                                    if ($property['MULTIPLE'] == 'Y' && $property['TYPE'] !== 'FILE') {
                                        $propertyList = unserialize($property["VALUE"]);
                                        foreach ($propertyList as $propertyElement) {
                                            echo $propertyElement . '</br>';
                                        }
                                    } else {
                                        echo $property["VALUE"];
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <?
                    }
                }
                ?>
                <tr>
                    <td><br></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2"><?= Loc::getMessage('SPOD_ORDER_SHIP_PAYMENT_PROPERTIES') ?></td>
                </tr>
                <tr>
                    <td><?= Loc::getMessage('SPOD_PAYMENT_SYSTEM') ?>:</td>
                    <td>
                        <?
                        foreach ($arResult['PAYMENT'] as $payment) {
                            echo $payment['PAY_SYSTEM_NAME'];
                        } ?>

                    </td>
                </tr>
                <tr>
                    <td><?= Loc::getMessage('SPOD_ORDER_PAYED') ?>:</td>
                    <td>
                        <?
                        if ($payment['PAID'] === 'Y') {
                            ?>

                            <?= Loc::getMessage('SPOD_PAYMENT_PAID') ?>
                            <?
                        } else {
                            ?>

                            <?= Loc::getMessage('SPOD_PAYMENT_UNPAID') ?>
                            <?
                        }
                        ?>                                                                                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>

                <tr>
                    <td><br></td>
                    <td></td>
                </tr>
                <tr>
                    <td><?= Loc::getMessage('SPOD_ORDER_DELIVERY') ?>:</td>
                    <td>
                        <? foreach ($arResult['SHIPMENT'] as $shipment) {

                            echo htmlspecialcharsbx($shipment["DELIVERY_NAME"]);
                        } ?>

                    </td>
                </tr>


                <tr>
                    <td><?= Loc::getMessage('SPOD_ORDER_SHIPMENT_BASKET') ?>:</td>
                    <td>
                        <?
                        foreach ($arResult['SHIPMENT'] as $shipment) {
                            foreach ($shipment['ITEMS'] as $item) {
                                $basketItem = $arResult['BASKET'][$item['BASKET_ID']];
                                ?>

                                <?= htmlspecialcharsbx($basketItem['NAME']) ?> (<?= $item['QUANTITY'] ?> <?= htmlspecialcharsbx($item['MEASURE_NAME']) ?>)
                                <br>
                                <?
                            }
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><br></td>
                    <td></td>
                </tr>


                </tbody>
            </table>
            <h3><?= Loc::getMessage('SPOD_ORDER_BASKET') ?></h3>

            <table class="bx_order_list_table_order">
                <thead>
                <tr>
                    <td colspan="2"><?= Loc::getMessage('SPOD_NAME') ?></td>
                    <td class="custom price"><?= Loc::getMessage('SPOD_PRICE') ?></td>
                    <td class="custom amount"><?= Loc::getMessage('SPOD_PRICETYPE') ?></td>
                    <td class="custom price"><?= Loc::getMessage('SPOD_QUANTITY') ?></td>
                </tr>
                </thead>
                <tbody>


                    <?foreach ($arResult['BASKET'] as $basketItem) {
                    ?>
                    <tr>
                        <td class="custom img">
                            <a href="<?= $basketItem['DETAIL_PAGE_URL'] ?>" target="_blank">

                                <?
                                if (strlen($basketItem['PICTURE']['SRC'])) {
                                    $imageSrc = $basketItem['PICTURE']['SRC'];
                                } else {
                                    $imageSrc = $this->GetFolder() . SITE_TEMPLATE_PATH.'/images/no_photo.png';
                                }
                                ?>
                                <img src="<?= $imageSrc ?>"
                                     width="82" height="110" alt="">

                            </a>
                        </td>
                        <td class="custom name">
                            <a href="<?= $basketItem['DETAIL_PAGE_URL'] ?>" target="_blank"><?= htmlspecialcharsbx($basketItem['NAME']) ?></a>
                        </td>

                        <td class="custom price"><span class="fm"><?= Loc::getMessage('SPOD_PRICE') ?>:</span><?= $basketItem['BASE_PRICE_FORMATED'] ?></td>


                        <td class="custom amount"><span class="fm">Тип цены:</span> Розничная цена</td>
                        <td class="custom price">
                            <span class="fm"><?= Loc::getMessage('SPOD_QUANTITY') ?>:</span>
                            <?= $basketItem['QUANTITY'] ?>&nbsp;
                            <?
                            if (strlen($basketItem['MEASURE_NAME'])) {
                                echo htmlspecialcharsbx($basketItem['MEASURE_NAME']);
                            } else {
                                echo Loc::getMessage('SPOD_DEFAULT_MEASURE');
                            }
                            ?>
                        </td>
                    </tr>
                    <?
                    }
                    ?>
                </tbody>
            </table>
            <br>
            <table class="bx_ordercart_order_sum">
                <tbody>


                <tr>
                    <td class="custom_t1"><?=Loc::getMessage('SPOD_COMMON_SUM') ?>:</td>
                    <td class="custom_t2"><?=$arResult['PRODUCT_SUM_FORMATED'] ?></td>
                </tr>

                <tr>
                    <td class="custom_t1"><?=Loc::getMessage('SPOD_DELIVERY') ?>:</td>
                    <td class="custom_t2"><?=$arResult["PRICE_DELIVERY_FORMATED"] ?></td>
                </tr>




                <tr>
                    <td class="custom_t1 fwb"><?=Loc::getMessage('SPOD_SUMMARY') ?>:</td>
                    <td class="custom_t2 fwb"><?=$arResult['PRICE_FORMATED'] ?></td>
                </tr>
                </tbody>
            </table>
            <br>

            <table class="bx_control_table" style="width: 100%;">
                <tbody>
                <tr>
                    <td>
                        <a href="<?= htmlspecialcharsbx($arResult["URL_TO_LIST"]) ?>" class="bx_big bx_bt_button_type_2 bx_cart"><?= Loc::getMessage('SPOD_GO_BACK')?>
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div><!--sale-order-detail-general-->

    <?
    $javascriptParams = array(
        "url" => CUtil::JSEscape($this->__component->GetPath() . '/ajax.php'),
        "templateFolder" => CUtil::JSEscape($templateFolder),
        "paymentList" => $paymentData
    );
    $javascriptParams = CUtil::PhpToJSObject($javascriptParams);
    ?>
    <script>
        BX.Sale.PersonalOrderComponent.PersonalOrderDetail.init(<?=$javascriptParams?>);
    </script>
    <?
}
?>

