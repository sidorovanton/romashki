<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
    <div class="bx-auth-profile">

        <?ShowError($arResult["strProfileError"]);?>
        <?
        if ($arResult['DATA_SAVED'] == 'Y')
            ShowNote(GetMessage('PROFILE_DATA_SAVED'));
        ?>
        <script type="text/javascript">
            <!--
            var opened_sections = [<?
                $arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
                $arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
                if (strlen($arResult["opened"]) > 0)
                {
                    echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
                }
                else
                {
                    $arResult["opened"] = "reg";
                    echo "'reg'";
                }
                ?>];
            //-->

            var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
        </script>

        <div class="h2 mt_0 mb_2">Мои настройки</div>
        <form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />

            <div class="profile-block-<?=strpos($arResult["opened"], "reg") === false ? "hidden" : "shown"?>" id="user_div_reg">




                <div class="col-xs-12 col-sm-4 text_right_sm"><?=GetMessage('NAME')?></div>
                <div class="col-xs-12 col-sm-5">
                    <input type="text" class="form-control" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" />
                </div>


                <div class="col-xs-12 col-sm-4 text_right_sm"><?=GetMessage('LAST_NAME')?></div>
                <div class="col-xs-12 col-sm-5">
                    <input type="text" class="form-control" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
                </div>

                <div class="col-xs-12 col-sm-4 text_right_sm"><?=GetMessage('SECOND_NAME')?></div>
                <div class="col-xs-12 col-sm-5">
                    <input type="text" class="form-control" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" />
                </div>

                <div class="col-xs-12 col-sm-4 text_right_sm">
                    <?=GetMessage('EMAIL')?><?if($arResult["EMAIL_REQUIRED"]):?>*<?endif?>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <input type="text" class="form-control" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
                </div>

                <div class="col-xs-12 col-sm-4 text_right_sm">
                    <?=GetMessage('LOGIN')?>*
                </div>
                <div class="col-xs-12 col-sm-5">
                    <input type="text" class="form-control" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
                </div>

                <?if($arResult["arUser"]["EXTERNAL_AUTH_ID"] == ''):?>
                    <div class="col-xs-12 col-sm-4 text_right_sm">
                        <?=GetMessage('NEW_PASSWORD_REQ')?>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <input type="password" class="form-control" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" class="bx-auth-input" />
                    </div>
                <?if($arResult["SECURE_AUTH"]):?>
                    <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
                    <noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                    </noscript>
                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure').style.display = 'inline-block';
                    </script>


                <?endif?>
                    <div class="col-xs-12 col-sm-4 text_right_sm">
                        <?=GetMessage('NEW_PASSWORD_CONFIRM')?>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <input type="password" class="form-control" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" />
                    </div>

                <?endif?>





                <div class="col-xs-12 col-sm-4 text_right_sm"><?=GetMessage("USER_BIRTHDAY_DT")?></div>
                <div class="col-xs-12 col-sm-5"><?
                    $APPLICATION->IncludeComponent(
                        'bitrix:main.calendar',
                        '',
                        array(
                            'SHOW_INPUT' => 'Y',
                            'FORM_NAME' => 'form1',
                            'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                            'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                            'SHOW_TIME' => 'N'
                        ),
                        null,
                        array('HIDE_ICONS' => 'Y')
                    );

                    //=CalendarDate("PERSONAL_BIRTHDAY", $arResult["arUser"]["PERSONAL_BIRTHDAY"], "form1", "15")
                    ?>
                </div>

                <div class="col-xs-12 col-sm-4 text_right_sm">
                    <?=GetMessage('USER_PHONE')?>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <input type="text" class="form-control" name="PERSONAL_PHONE" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" />
                </div>




                <div class="col-xs-12 col-sm-4 text_right_sm">
                    <?=GetMessage('USER_CITY')?>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <input type="text" class="form-control" name="PERSONAL_CITY" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" />
                </div>


                <div class="col-xs-12 col-sm-4 text_right_sm">
                    <?=GetMessage("USER_STREET")?>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <textarea cols="10" rows="3" class="form-control" name="PERSONAL_STREET"><?=$arResult["arUser"]["PERSONAL_STREET"]?></textarea>
                </div>
                <div class="clear"></div>

            </div>


            <?
            if ($arResult["INCLUDE_FORUM"] == "Y")
            {
                ?>



                <?
            }
            ?>
            <?
            if ($arResult["INCLUDE_BLOG"] == "Y")
            {
                ?>

                <?
            }
            ?>
            <?if ($arResult["INCLUDE_LEARNING"] == "Y"):?>

            <?endif;?>
            <?if($arResult["IS_ADMIN"]):?>

            <?endif;?>
            <?// ********************* User properties ***************************************************?>
            <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                <div class="profile-link profile-user-div-link"><a title="<?=GetMessage("USER_SHOW_HIDE")?>" href="javascript:void(0)" onclick="SectionClick('user_properties')"><?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></a></div>
                <div id="user_div_user_properties" class="profile-block-<?=strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown"?>">
                    <table class="data-table profile-table">
                        <thead>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?$first = true;?>
                        <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
                            <tr><td class="field-name">
                                    <?if ($arUserField["MANDATORY"]=="Y"):?>
                                        <span class="starrequired">*</span>
                                    <?endif;?>
                                    <?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td class="field-value">
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:system.field.edit",
                                        $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                        array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
                        <?endforeach;?>
                        </tbody>
                    </table>
                </div>
            <?endif;?>
            <?// ******************** /User properties ***************************************************?>
            <!--<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>-->

            <div class="text_center mt_2">
                <input class="btn btn-primary" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                <input class="btn btn-primary" type="reset" value="<?=GetMessage('MAIN_RESET');?>">
            </div>
        </form>
    </div>
