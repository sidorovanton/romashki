<?
//if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//echo 'Post<pre>' . print_r($_POST, true) . '</pre>';
global $APPLICATION;
$APPLICATION->IncludeComponent("bitrix:main.register", "ajax", Array(
    "AUTH" => "Y",    // Автоматически авторизовать пользователей
    "REQUIRED_FIELDS" => array(    // Поля, обязательные для заполнения
        0 => "EMAIL",
    ),
    "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
    "SHOW_FIELDS" => array(    // Поля, которые показывать в форме
        0 => "EMAIL",
        1 => "NAME",
        2 => "LAST_NAME",
    ),
    "SUCCESS_PAGE" => "/",    // Страница окончания регистрации
    "USER_PROPERTY" => "",    // Показывать доп. свойства
    "USER_PROPERTY_NAME" => "",    // Название блока пользовательских свойств
    "USE_BACKURL" => "Y",    // Отправлять пользователя по обратной ссылке, если она есть
),
    false
); ?>
