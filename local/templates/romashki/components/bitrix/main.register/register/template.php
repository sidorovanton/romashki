<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

?>
<? if (!$USER->IsAuthorized()): ?>
    / <a class="Lora-BoldItalic open-popup-inline" data-effect="mfp-zoom-in"
           href="#popup-registration">Регистрация</a>
    <div id="popup-registration" data-effect="mfp-zoom-in" class=" white-popup mfp-with-anim mfp-hide clearfix">
        <div class="popup_header">
            <div class="title_line_horizontal title_line_decor">
                <span>Регистрация</span>
            </div>
        </div>
        <div id='register-error' style="color : red">
        </div>

        <div class="col-xs-offset-1 col-xs-10 popup_wrap">
            <form class="clearfix" method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform"
                  enctype="multipart/form-data" id='regform'>
                <div class="form-group">
                    <label for="name">Имя:</label>
                    <input type="text" class="form-control" id="name" placeholder="" name="REGISTER[NAME]"
                           value="<?= $arResult["VALUES"]["NAME"] ?>">
                </div>
                <div class="form-group">
                    <label for="fam">Фамилия:</label>
                    <input type="text" class="form-control" id="fam" placeholder="" name="REGISTER[LAST_NAME]"
                           value="<?= $arResult["VALUES"]["LAST_NAME"] ?>">
                </div>
                <div class="form-group has-error">
                    <label for="login"> Логин (мин. 3 символа):<span class="color_danger">*</span></label>
                    <input type="text" class="form-control" id="login" placeholder="" name="REGISTER[LOGIN]"
                           value="<?= $arResult["VALUES"]["LOGIN"] ?>">
                </div>

                <div class="form-group has-error">
                    <label for="pass"> Пароль:<span class="color_danger">*</span></label>
                    <input type="text" class="form-control" id="pass" name="REGISTER[PASSWORD]"
                           value="<?= $arResult["VALUES"]["PASSWORD"] ?>" placeholder="" autocomplete="off">
                </div>
                <input value="Y" type="hidden" name="register_submit_button">
                <div class="form-group">
                    <label for="passpod">Подтверждение пароля:<span class="color_danger">*</span></label>
                    <input type="text" class="form-control" id="passpod" placeholder=""
                           name="REGISTER[CONFIRM_PASSWORD]" value="<?= $arResult["VALUES"]["CONFIRM_PASSWORD"] ?>"
                           autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="mail">Адрес e-mail:<span class="color_danger">*</span></label>
                    <input type="text" class="form-control" id="mail" placeholder="" name="REGISTER[EMAIL]"
                           value="<?= $arResult["VALUES"]["EMAIL"] ?>">
                </div>
                <?
                /* CAPTCHA */
                if ($arResult["USE_CAPTCHA"] == "Y") {
                    ?>

                    <div class="form-group">
                        <label for="captcha"><?= GetMessage("REGISTER_CAPTCHA_TITLE") ?><span
                                    class="color_danger">*</span></label>
                        <div class="bx-captcha"><img
                                    src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>"
                                    width="180" height="40" alt="CAPTCHA"></div>
                        <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                        <input type="text" class="form-control" id="captcha" placeholder="" name="captcha_word"
                               maxlength="50" value="">
                    </div>
                    <?
                }
                /* !CAPTCHA */
                ?>

                <div class="col-xs-offset-2 col-xs-8 text_center mt_2 mb_1">
                    <input class="btn btn-primary full_width _upper" value="Регистрация" type="submit"
                           name="register_submit_button"/>
                </div>

            </form>
            <hr/>
            <div class="text-center fz_12">
                Пароль не должен быть менее 6 символов. <br/>
                <span class="color_danger">*</span> Поля, обязательные для заполнения.<br/>
                <a class="open-popup-inline" href="#popup-enter">Авторизация</a>
            </div>

        </div>

    </div>
<? endif; ?>