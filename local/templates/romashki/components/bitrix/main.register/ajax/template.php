<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
global $USER;/*
$user = new CUser();
foreach ($_POST["REGISTER"] as $key => $value)
    $fields[$key] =  $value;
$ID = $user->Add($fields);
$APPLICATION->RestartBuffer();
//echo 'Result<pre>' . print_r($arResult, true) . '</pre>';
//echo 'Params<pre>' . print_r($arParams, true) . '</pre>';
//echo 'Post<pre>' . print_r($_POST, true) . '</pre>';
//echo 'ByLogin<pre>' . print_r($USER->GetByxLogin($_POST['REGISTER']['LOGIN'])->Fetch(), true) . '</pre>';
if (intval($ID) > 0) {
    $user->Authorize($ID);

}*/
//echo 'Result<pre>' . print_r($arResult, true) . '</pre>';
if (intval($arResult["VALUES"]["USER_ID"]))
    die('Y'); // если авторизация прошла успешно, возвращаем Y
// в противном случае нам нужно вернуть html с описанием ошибок
if (count($arResult["ERRORS"]) > 0) {
    foreach ($arResult["ERRORS"] as $key => $error)
        if (intval($key) == 0 && $key !== 0)
            $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
    die(implode("<br />", $arResult["ERRORS"]));
} else {
// ну а если описание ошибок отсутствует, вернем простое служебное сообщение,
// чтобы не держать пользователя в неведении

}
?>