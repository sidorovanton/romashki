<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="popup-rduction" data-effect="mfp-zoom-in" class="mfp-with-anim white-popup mfp-hide clearfix">
    <div class="popup_header">
        <div class="title_line_horizontal title_line_decor">
            <span>Восстановить пароль</span>
        </div>
    </div>
    <div class="col-xs-offset-1 col-xs-10 popup_wrap">
        <form class="clearfix" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
            <?
            if (strlen($arResult["BACKURL"]) > 0)
            {
                ?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                <?
            }
            ?>
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="SEND_PWD">
            <div class="form-group">
                <label for="login">Введите Ваше имя</label>
                <input type="text" class="form-control" name = "USER_LOGIN" id="login" placeholder="">
            </div>
            <div class="form-group">
                <label for="pass">Введите Ваш e-mail</label>
                <input type="text" name = "USER_EMAIL" class="form-control" id="pass" placeholder="">
            </div>

            <div class= "col-xs-offset-2 col-xs-8 text_center mt_2 mb_2">
                <input type="hidden" name="send_account_info" value="Y" />
                <input class="btn btn-primary full_width _upper" value="Отправить" type="submit"/>
            </div>
        </form>
    </div>
</div>
