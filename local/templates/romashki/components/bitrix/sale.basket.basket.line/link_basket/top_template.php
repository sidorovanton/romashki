<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
?>
<div class="basket">
    <span class="basket_icon<?if($arResult['NUM_PRODUCTS']):?> active<?endif?>"></span>
    <div class="basket_text">
        <a class="basket__lnk lnk_brd" href="<?=$arParams['PATH_TO_BASKET']?>"><?=GetMessage('TSB1_CART');?> <span class="">(<?=$arResult['NUM_PRODUCTS']?>)</span></a>
        <div class="basket_sum"><strong><?= $arResult['TOTAL_PRICE'] ?></strong></div>
    </div>
</div>