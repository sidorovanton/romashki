<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var string $strElementEdit */
/** @var string $strElementDelete */
/** @var array $arElementDeleteParams */
/** @var array $skuTemplate */
/** @var array $templateData */
global $APPLICATION;
?>
<div class="bx_catalog_top_home col<? echo $arParams['LINE_ELEMENT_COUNT']; ?> <? echo $templateData['TEMPLATE_CLASS']; ?>">
<?
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'MAIN_PROPS' => $strMainID.'_main_props',

		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',

		'PRICE' => $strMainID.'_price',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',

		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop'
	);

	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);
	$imgTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $arItem['NAME']
	);

	$minPrice = false;
	if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
		$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
?>
	<div class="col-xs-12 col-mid-xs-6 col-sm-4 col-md-3" id="<? echo $strMainID; ?>">
		<div class="card_preview">
			<div class="card_preview__wrap_hidden">
				<a id="<? echo $arItemIDs['PICT']; ?>" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="card_preview__lnk_pic">
					<img class="card_preview__pic" src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" title="<? echo $imgTitle; ?>" alt="img">
					<?
					if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {
						?>
						<span class="label_card label_right">
							<span id="<? echo $arItemIDs['DSC_PERC']; ?>" class="label_info _new"></span><br>
                        </span>
						<?
					}?>
					<span class="label_card">
						<?if ($arItem["DISPLAY_PROPERTIES"]['NEWPRODUCT']){?>
							<span class="label_info _hit"></span><br>
						<?}
						if ($arItem['LABEL']) {
							?><span class="label_info _discount"></span><?
						}?>
					</span>
				</a>
				<div class="card_preview__info ">
					<div class="raiting text_center">
						<? $APPLICATION->IncludeComponent(
							"bitrix:iblock.vote",
							"flat",
							Array(
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"DISPLAY_AS_RATING" => "rating",
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"ELEMENT_ID" => $arItem["ID"],
								"MAX_VOTE" => $arParams["MAX_VOTE"],
								"VOTE_NAMES" => $arParams["VOTE_NAMES"],
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"SHOW_RATING" => "Y",
							),
							$component
						); ?>
					</div>
					<div class="card_preview__prod_name">
						<a class="card_preview__lnk lnk_brd" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>"><? echo $productTitle; ?></a>
					</div>
					<div id="<? echo $arItemIDs['PRICE']; ?>" class="card_preview__price">
						<?
						if (!empty($minPrice)) {
							if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
								echo GetMessage(
									'CT_BCT_TPL_MESS_PRICE_SIMPLE_MODE',
									array(
										'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE'],
										'#MEASURE#' => GetMessage(
											'CT_BCT_TPL_MESS_MEASURE_SIMPLE_MODE',
											array(
												'#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
												'#UNIT#' => $minPrice['CATALOG_MEASURE_NAME']
											)
										)
									)
								);
							} else {
								echo $minPrice['PRINT_DISCOUNT_VALUE'];
							}
							if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {
								?>
								<span class="card_preview__price_old"><? echo $minPrice['PRINT_VALUE']; ?></span>
								<?
							}
						}
						unset($minPrice);
						?>
					</div>

					<a href="javascript:void(0)" class="wish" data-effect="mfp-zoom-in"
					   onclick='addProdDelay(
						"<?=$arItem["ID"]?>",
						"<?=$arItem["CATALOG_PRICE_ID_1"]?>",
						"<?=$arItem["MIN_PRICE"]["DISCOUNT_VALUE"]?>",
						"<?=$arItem["NAME"]?>",
						"<?=$arItem["DETAIL_PAGE_URL"]?>",
                               "<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>",
                               "<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>"


						)'>
						<i class="icon i_star"></i>Добавить в избранное
					</a>
				</div>
				<?
				if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS']))
				{?>
                    <div class="bx_catalog_item_controls"><?
					if ($arItem['CAN_BUY']) {
						?>
						<div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>">
							<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="btn btn-primary full_width _upper" href="javascript:void(0)" rel="nofollow">
								<?
								if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {
									echo('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCT_TPL_MESS_BTN_BUY'));
								} else {
									echo('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCT_TPL_MESS_BTN_ADD_TO_BASKET'));
								}
								?>
							</a>
						</div>
						<?
					}
					else {
						?>
						<div id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>">
							<span>
								<?
									echo('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessage('CT_BCT_TPL_MESS_PRODUCT_NOT_AVAILABLE'));
								?>
							</span>
						</div>
					<?
					}?>
                        <div style="clear: both;"></div>
                    </div><?
					$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
					if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
					{
					?>
						<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
							<?
							if (!empty($arItem['PRODUCT_PROPERTIES_FILL'])) {
								foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
									?>
									<input type="hidden"
										   name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
										   value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>"><?
									if (isset($arItem['PRODUCT_PROPERTIES'][$propID]))
										unset($arItem['PRODUCT_PROPERTIES'][$propID]);
								}
							}
							$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
							if (!$emptyProductProperties) {
								?>
								<table>
									<?
									foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
										?>
										<tr>
											<td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
											<td>
												<?
												if (
													'L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE']
													&& 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']
												) {
													foreach ($propInfo['VALUES'] as $valueID => $value) {
														?><label><input type="radio"
																		name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
																		value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?>
														</label><br><?
													}
												} else {
													?>
													<select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">
														<?
														foreach ($propInfo['VALUES'] as $valueID => $value) {
															?><option	value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
														}
													?>
													</select>
													<?
												}
												?>
											</td>
										</tr>
										<?
									}
									?>
								</table>
								<?
							}
							?>
						</div>
					<?
					}
					$arJSParams = array(
						'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
						'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
						'SHOW_ADD_BASKET_BTN' => false,
						'SHOW_BUY_BTN' => true,
						'SHOW_ABSENT' => true,
						'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
						'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
						'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
						'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
						'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
						'PRODUCT' => array(
							'ID' => $arItem['ID'],
							'NAME' => $productTitle,
							'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
							'CAN_BUY' => $arItem["CAN_BUY"],
							'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
							'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
							'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
							'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
							'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
							'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
							'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
						),
						'VISUAL' => array(
							'ID' => $arItemIDs['ID'],
							'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
							'QUANTITY_ID' => $arItemIDs['QUANTITY'],
							'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
							'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
							'PRICE_ID' => $arItemIDs['PRICE'],
							'BUY_ID' => $arItemIDs['BUY_LINK'],
							'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
							'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
							'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
							'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
						),
						'BASKET' => array(
							'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
							'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
							'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
							'EMPTY_PROPS' => $emptyProductProperties,
							'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
							'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
						),
						'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
					);
					?>
					<script type="text/javascript">
						var <? echo $strObName; ?>= new JCCatalogTopSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
					</script>
					<?
					}
				else
				{
					?>
					<div>
						<a class="btn btn-primary full_width _upper" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?
							echo ('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCT_TPL_MESS_BTN_DETAIL'));
							?>
						</a>
					</div>
					<?
					if ('Y' == $arParams['PRODUCT_DISPLAY_MODE'])
					{
						if (!empty($arItem['OFFERS_PROP']))
						{
							$arJSParams = array(
								'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
								'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
								'SHOW_ADD_BASKET_BTN' => false,
								'SHOW_BUY_BTN' => true,
								'SHOW_ABSENT' => true,
								'SHOW_SKU_PROPS' => $arItem['OFFERS_PROPS_DISPLAY'],
								'SECOND_PICT' => $arItem['SECOND_PICT'],
								'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
								'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
								'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
								'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
								'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
								'DEFAULT_PICTURE' => array(
									'PICTURE' => $arItem['PRODUCT_PREVIEW'],
									'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
								),
								'VISUAL' => array(
									'ID' => $arItemIDs['ID'],
									'PICT_ID' => $arItemIDs['PICT'],
									'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
									'QUANTITY_ID' => $arItemIDs['QUANTITY'],
									'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
									'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
									'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
									'PRICE_ID' => $arItemIDs['PRICE'],
									'TREE_ID' => $arItemIDs['PROP_DIV'],
									'TREE_ITEM_ID' => $arItemIDs['PROP'],
									'BUY_ID' => $arItemIDs['BUY_LINK'],
									'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
									'DSC_PERC' => $arItemIDs['DSC_PERC'],
									'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
									'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
									'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
									'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
									'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
								),
								'BASKET' => array(
									'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
									'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
									'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
									'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
									'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
								),
								'PRODUCT' => array(
									'ID' => $arItem['ID'],
									'NAME' => $productTitle
								),
								'OFFERS' => $arItem['JS_OFFERS'],
								'OFFER_SELECTED' => $arItem['OFFERS_SELECTED'],
								'TREE_PROPS' => $arSkuProps,
								'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
							);
							if ($arParams['DISPLAY_COMPARE'])
							{
								$arJSParams['COMPARE'] = array(
									'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
									'COMPARE_PATH' => $arParams['COMPARE_PATH']
								);
							}
							?>
								<script type="text/javascript">
									var <? echo $strObName; ?> = new JCCatalogTopSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
								</script>
							<?
						}
					}
					else
					{
						$arJSParams = array(
							'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
							'SHOW_QUANTITY' => false,
							'SHOW_ADD_BASKET_BTN' => false,
							'SHOW_BUY_BTN' => false,
							'SHOW_ABSENT' => false,
							'SHOW_SKU_PROPS' => false,
							'SECOND_PICT' => $arItem['SECOND_PICT'],
							'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
							'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
							'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
							'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
							'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
							'DEFAULT_PICTURE' => array(
								'PICTURE' => $arItem['PRODUCT_PREVIEW'],
								'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
							),
							'VISUAL' => array(
								'ID' => $arItemIDs['ID'],
								'PICT_ID' => $arItemIDs['PICT'],
								'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
								'QUANTITY_ID' => $arItemIDs['QUANTITY'],
								'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
								'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
								'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
								'PRICE_ID' => $arItemIDs['PRICE'],
								'TREE_ID' => $arItemIDs['PROP_DIV'],
								'TREE_ITEM_ID' => $arItemIDs['PROP'],
								'BUY_ID' => $arItemIDs['BUY_LINK'],
								'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
								'DSC_PERC' => $arItemIDs['DSC_PERC'],
								'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
								'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
								'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
								'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
								'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
							),
							'BASKET' => array(
								'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
								'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
								'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
								'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
								'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
							),
							'PRODUCT' => array(
								'ID' => $arItem['ID'],
								'NAME' => $productTitle
							),
							'OFFERS' => array(),
							'OFFER_SELECTED' => 0,
							'TREE_PROPS' => array(),
							'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
						);
						if ($arParams['DISPLAY_COMPARE'])
						{
							$arJSParams['COMPARE'] = array(
								'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
								'COMPARE_PATH' => $arParams['COMPARE_PATH']
							);
						}
						?>
						<script type="text/javascript">
							var <? echo $strObName; ?> = new JCCatalogTopSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
						</script>
						<?
					}
				}
				?>
			</div>
		</div>
	</div><?
}
?>
<div style="clear: both;"></div>
</div>