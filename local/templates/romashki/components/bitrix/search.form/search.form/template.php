<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true); ?>



<div class="header_search mb_1 ">
    <form action="<?= $arResult["FORM_ACTION"] ?>" method="get">
        <input class="header_search__input" type="text" placeholder="<?= GetMessage('BSF_T_SEARCH_BUTTON'); ?>" name="q"
               maxlength="50">
        <span class="header_search__submit icon icon-search">
                                            <input class="btn-search" name="btn-search" type="submit" value="">
                                        </span>
    </form>
</div>