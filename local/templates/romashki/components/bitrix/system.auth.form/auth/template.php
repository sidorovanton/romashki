<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
CJSCore::Init(array("jquery"));
?>
<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) {

}
?>
<? global $USER;
?>
<? if ($arResult["FORM_TYPE"] == "login" && !$USER->IsAuthorized()): ?>
    <a class="Lora-BoldItalic open-popup-inline" data-effect="mfp-zoom-in" href="#popup-enter">Войти</a>
    <script type="text/javascript"
            src="<?= SITE_TEMPLATE_PATH ?>/components/bitrix/system.auth.form/auth/auth.js"></script>
    <div id="popup-enter" data-effect="mfp-zoom-in" class="mfp-with-anim white-popup mfp-hide clearfix">
        <div class="popup_header">
            <div class="title_line_horizontal title_line_decor">
                <span>Войти</span>
            </div>
        </div>
        <div class="col-xs-offset-1 col-xs-10 popup_wrap">
            <div id='auth-error' style="color : red">
            </div>
            <form class="clearfix" id="login-form" name="system_auth_form<?= $arResult["RND"] ?>" method="post"
                  target="_top"
                  action="<?= $arResult["AUTH_URL"] ?>">
                <? if ($arResult["BACKURL"] <> ''): ?>
                    <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                <? endif ?>
                <? foreach ($arResult["POST"] as $key => $value): ?>
                    <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
                <? endforeach ?>
                <input type="hidden" name="AUTH_FORM" value="Y"/>
                <input type="hidden" name="TYPE" value="AUTH"/>

                <div class="form-group">
                    <label for="login">Логин</label>
                    <input type="text" name="USER_LOGIN" class="form-control" id="login" placeholder=""
                           value="<?= $arResult["USER_LOGIN"] ?>">
                </div>
                <div class="form-group">
                    <label for="pass">Пароль</label>
                    <input type="password" name="USER_PASSWORD" class="form-control" id="pass" placeholder=""
                           autocomplete="off">
                </div>
            <? if ($arResult["STORE_PASSWORD"] == "Y"): ?>
                <input id="check" type="checkbox" name="USER_REMEMBER" value="Y"/>
                    <label class="font_normal" for="check">Запомнить меня на этом компьютере</label>
                    <? endif ?>
                    <div class="col-xs-offset-2 col-xs-8 text_center mt_2 mb_2">
                        <input class="btn btn-primary full_width _upper" value="Войти" type="submit"/>
                    </div>
            </form>

        <div class="text-center">
            <a class="fz_12 open-popup-inline" href="#popup-rduction">Забыли пароль?</a>
            <a class=" fz_12 open-popup-inline" href="#popup-registration">Зарегистрироваться</a>
        </div>
        </div>
    </div>
<? else: ?>
    <?= $arResult["USER_LOGIN"] ?> <a href="?logout=yes">Выйти</a>
<? endif; ?>

