$(document).ready(function () {
    // ловим событие отправки формы
    $('#login-form').submit(function () {
        var path = '/local/templates/romashki/components/bitrix/system.auth.form/auth/auth.php';
        var formData = $(this).serialize();

      //  alert(formData);
        var success = function (response) {
            if (response == 'Y') {
                location.reload();
                $('#auth-error').html('<span style = "color:green">Авторизация прошла успешно</span>').show();
            }
            else {
                $('#auth-error').html(response).show();
            }
        };

        var responseType = 'html';
        $.post(path, formData, success, responseType);
        return false;
    });
    $('#regform').submit(function () {
        var path = '/local/templates/romashki/components/bitrix/main.register/register/register.php';
        var formData = $(this).serialize();
        $('#auth-error').html(formData).show();

        var success = function (response) {
            if (response == 'Y') {
                location.reload();
                $('#register-error').html('<span style = "color:green">Регистрация прошла успешно</span>').show();
            }
            else {
                $('#register-error').html(response).show();
            }
        };

        var responseType = 'html';
        $.post(path, formData, success, responseType);
        return true;
    });
});