<?
//if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION;
$APPLICATION->IncludeComponent(
    "bitrix:system.auth.form",
    "ajax",
    Array(
        "COMPONENT_TEMPLATE" => ".default",
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => "/personal/cart/",
        "REGISTER_URL" => "",
        "SHOW_ERRORS" => "Y"
    )
); ?>
