<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$id_circle_news = 0;
?>
<div class="benefits" id="benefits">
    <div class="container">
        <div class="benefits-content">
            <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
                <?= $arResult["NAV_STRING"] ?><br/>
            <? endif; ?>
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
                    <h3 class="bx-newslist-title">
                        <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                            <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
                        <?else:?>
                            <?echo $arItem["NAME"]?>
                        <?endif;?>
                    </h3>
                <?endif;?>
                <?if($arParams["DISPLAY_PICTURE"]!="N"):?>
                    <?if ($arItem["VIDEO"]):?>
                        <div class="bx-newslist-youtube embed-responsive embed-responsive-16by9" style="display: block;">
                            <iframe
                                src="<?echo $arItem["VIDEO"]?>"
                                frameborder="0"
                                allowfullscreen=""
                            ></iframe>
                        </div>
                    <?elseif ($arItem["SOUND_CLOUD"]):?>
                        <div class="bx-newslist-audio">
                            <iframe
                                width="100%"
                                height="166"
                                scrolling="no"
                                frameborder="no"
                                src="https://w.soundcloud.com/player/?url=<?echo urlencode($arItem["SOUND_CLOUD"])?>&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"
                            ></iframe>
                        </div>
                    <?elseif ($arItem["SLIDER"] && count($arItem["SLIDER"]) > 1):?>
                        <div class="bx-newslist-slider">
                            <div class="bx-newslist-slider-container" style="width: <?echo count($arItem["SLIDER"])*100?>%;left: 0;">
                                <?foreach ($arItem["SLIDER"] as $file):?>
                                    <div style="width: <?echo 100/count($arItem["SLIDER"])?>%;" class="bx-newslist-slider-slide">
                                        <img src="<?=$file["SRC"]?>" alt="<?=$file["DESCRIPTION"]?>">
                                    </div>
                                <?endforeach?>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="bx-newslist-slider-arrow-container-left"><div class="bx-newslist-slider-arrow"><i class="fa fa-angle-left" ></i></div></div>
                            <div class="bx-newslist-slider-arrow-container-right"><div class="bx-newslist-slider-arrow"><i class="fa fa-angle-right"></i></div></div>
                            <ul class="bx-newslist-slider-control">
                                <?foreach ($arItem["SLIDER"] as $i => $file):?>
                                    <li rel="<?=($i+1)?>" <?if (!$i) echo 'class="current"'?>><span></span></li>
                                <?endforeach?>
                            </ul>
                        </div>
                        <script type="text/javascript">
                            BX.ready(function() {
                                new JCNewsSlider('<?=CUtil::JSEscape($this->GetEditAreaId($arItem['ID']));?>', {
                                    imagesContainerClassName: 'bx-newslist-slider-container',
                                    leftArrowClassName: 'bx-newslist-slider-arrow-container-left',
                                    rightArrowClassName: 'bx-newslist-slider-arrow-container-right',
                                    controlContainerClassName: 'bx-newslist-slider-control'
                                });
                            });
                        </script>
                    <?elseif ($arItem["SLIDER"]):?>
                        <div class="bx-newslist-img">
                            <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
                                        src="<?=$arItem["SLIDER"][0]["SRC"]?>"
                                        width="<?=$arItem["SLIDER"][0]["WIDTH"]?>"
                                        height="<?=$arItem["SLIDER"][0]["HEIGHT"]?>"
                                        alt="<?=$arItem["SLIDER"][0]["ALT"]?>"
                                        title="<?=$arItem["SLIDER"][0]["TITLE"]?>"
                                    /></a>
                            <?else:?>
                                <img
                                    src="<?=$arItem["SLIDER"][0]["SRC"]?>"
                                    width="<?=$arItem["SLIDER"][0]["WIDTH"]?>"
                                    height="<?=$arItem["SLIDER"][0]["HEIGHT"]?>"
                                    alt="<?=$arItem["SLIDER"][0]["ALT"]?>"
                                    title="<?=$arItem["SLIDER"][0]["TITLE"]?>"
                                />
                            <?endif;?>
                        </div>
                    <?elseif (is_array($arItem["PREVIEW_PICTURE"])):?>
                        <div class="bx-newslist-img">
                            <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
                                        src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                        width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                                        height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                                        alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                                        title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                                        align="left"
                                    /></a>
                            <?else:?>
                                <img
                                    src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                    width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                                    height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                                    alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                                    title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                                    align="left"
                                />
                            <?endif;?>
                        </div>
                    <?endif;?>
                <?endif;?>
                <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                    <div class="position_content">
                        <div class="clearfix colelem">
                            <span><? echo $arItem["PREVIEW_TEXT"]; ?></span>
                        </div>
                    </div>
                <? endif; ?>
            <? endforeach; ?>
        </div>
    </div>
</div>

