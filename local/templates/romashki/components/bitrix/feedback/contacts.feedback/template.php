<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)?>
		<div class="col-xs-12 mf-nook-text"><?=$v?></div><?
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="col-xs-12 mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>
<div class="h3 col-xs-12 ">Свяжитесь с нами</div>
<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
<?=bitrix_sessid_post()?>
    <div class="col-xs-12 ">
		<input type="text" class="form-control" name="user_name" placeholder="<?=GetMessage('MFT_NAME');?>">
    </div>
    <div class="col-xs-12 ">
		<input type="text"  class="form-control" name="user_email" placeholder="<?=GetMessage('MFT_EMAIL');?>">
    </div>
    <div class="col-xs-12 ">
        <textarea class="form-control" cols="10" rows="3" name="user_message" placeholder="<?=GetMessage('MFT_MESSAGE');?>"></textarea>
    </div>


        <div class="clear"></div>
        <div class="text_center mt_2">
            <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
            <input type="submit" class="btn btn-lg btn-primary" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
        </div>
</form>






