<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <aside class="col-md-3 hidden-sm hidden-mid-xs hidden-xs">
        <nav class="aside_menu-2">
            <ul class="aside_menu-2__list">
                <?
                $intCurrentDepth = 1;
                $boolFirst = true;
                foreach ($arResult as $arItem):
                if ($intCurrentDepth < $arItem['DEPTH_LEVEL']) {
                    if (0 < $intCurrentDepth)
                        echo '<ul class="aside_menu-2__list-' . ($intCurrentDepth + 1) . '">';
                } elseif ($intCurrentDepth == $arItem['DEPTH_LEVEL']) {
                    if (!$boolFirst)
                        echo '</li>';
                } else {
                    while ($intCurrentDepth > $arItem['DEPTH_LEVEL']) {
                        echo '</li>', '</ul>';
                        $intCurrentDepth--;
                    }
                    echo '</li>';
                }
                ?>
                <?if($arItem["SELECTED"]):?>
                    <li class="active">
                <?else:?>
                    <li>
                <?endif?>
                    <a href="<? echo $arItem["LINK"]; ?>"><? echo $arItem["TEXT"]; ?></a>
                    <?
                    if ($arItem['IS_PARENT']){
                        ?><i class="icon_menu__caret"></i><?
                    }
                    else {
                        ?><i class="arr_mob_menu"></i><?
                    }
                    $intCurrentDepth = $arItem['DEPTH_LEVEL'];
                    $boolFirst = false;
                    endforeach;
                    unset($arSection);
                    while ($intCurrentDepth > 1) {
                        echo '</li>', '</ul>';
                        $intCurrentDepth--;
                    }
                    if ($intCurrentDepth > 0) {
                        echo '</li>';
                    }
                    ?>
            </ul>
        </nav>
    </aside>

<? endif ?>


