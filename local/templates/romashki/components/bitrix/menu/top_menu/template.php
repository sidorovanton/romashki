<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
    <div class="col-xs-12 col-sm-5 bdb_min-xs mid-xs_text_center">
    <a class="menu_collapse__btn visible-mid-xs" role="button" data-toggle="collapse" href="#collapsMenu"
       aria-expanded="false" aria-controls="collapsMenu">
        Меню
    </a>
    <div class=" menu_collapse__btn menu_collapse__btn-right menu-btn visible-mid-xs">Каталог</div>
    <div class="clear"></div>
<? if (!empty($arResult)): ?>
    <nav class="header_menu menu_collapse collapse" id="collapsMenu">
        <ul class="list_inline lnk_white">

            <?
            foreach ($arResult as $arItem):
                if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                    continue;
                ?>
                <? if ($arItem["SELECTED"]): ?>

                <li class="active">
                    <a class="lnk_style lnk_brd_bottom" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                </li>
            <? else: ?>
                <li>
                    <a class="lnk_style lnk_brd_bottom" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                </li>
            <? endif ?>

            <? endforeach ?>


    </nav>
    </div>
<? endif ?>