<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
CJSCore::Init(array('clipboard'));

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL'])) {
    foreach ($arResult['ERRORS']['FATAL'] as $error) {
        ShowError($error);
    }
    $component = $this->__component;
    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
        $APPLICATION->AuthForm('', false, false, 'N', false);
    }

} else {
    if (!empty($arResult['ERRORS']['NONFATAL'])) {
        foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
            ShowError($error);
        }
    }
    if (!count($arResult['ORDERS'])) {
        if ($_REQUEST["filter_history"] == 'Y') {
            if ($_REQUEST["show_canceled"] == 'Y') {
                ?>
                <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER') ?></h3>
                <?
            } else {
                ?>
                <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST') ?></h3>
                <?
            }
        } else {
            ?>
            <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST') ?></h3>
            <?
        }
    }
    ?>

    <div class="tab-content">
    <div class="h2 mt_0 mb_2">Мои заказы</div>
    <?
    $nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);
    $clearFromLink = array("filter_history", "filter_status", "show_all", "show_canceled");

    if ($nothing || $_REQUEST["filter_history"] == 'N') {
        ?>

        <a class="btn btn_reverse"
           href="<?= $APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false) ?>">
            <? echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY") ?>
        </a>
        <?
    }
    if ($_REQUEST["filter_history"] == 'Y') {
        ?>
        <a class="sale-order-history-link" href="<?= $APPLICATION->GetCurPageParam("", $clearFromLink, false) ?>">
            <? echo Loc::getMessage("SPOL_TPL_CUR_ORDERS") ?>
        </a>
        <?
        ?>
        <a class="sale-order-history-link"
           href="<?= $APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y", $clearFromLink, false) ?>">
            <? echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_CANCELED") ?>
        </a>
        <?
    }
    ?>
    <div role="tabpanel" class="tab-pane active" id="orders">
    <?

    if ($_REQUEST["filter_history"] !== 'Y') {
        $paymentChangeData = array();
        $orderHeaderStatus = null;

        foreach ($arResult['ORDERS'] as $key => $order) {

            if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID']) {
                $orderHeaderStatus = $order['ORDER']['STATUS_ID'];

                ?>


                <?
            }

            ?>
            <div class="h3">
                <?= Loc::getMessage('SPOL_TPL_ORDER_IN_STATUSES') ?> &laquo;<?= htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME']) ?>&raquo;
            </div>
            <div class="table-responsive">
                <table>
                    <tr>
                        <th>
                            <?= Loc::getMessage('SPOL_TPL_ORDER') ?>
                            <?= Loc::getMessage('SPOL_TPL_NUMBER_SIGN') . $order['ORDER']['ACCOUNT_NUMBER'] ?>
                            <?= Loc::getMessage('SPOL_TPL_FROM_DATE') ?>
                            <?= $order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT']) ?>
                        </th>
                        <th class="text-right">

                            <a href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"]) ?>"><?= Loc::getMessage('SPOL_TPL_MORE_ON_ORDER') ?></a>

                        </th>
                    </tr>
                    <tr>
                        <?
                        $showDelimeter = false;
                        foreach ($order['PAYMENT'] as $payment) {
                            $paymentChangeData[$payment['ACCOUNT_NUMBER']] = array(
                                "order" => htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']),
                                "payment" => htmlspecialcharsbx($payment['ACCOUNT_NUMBER'])
                            );
                            ?>

                            <td>
                                <ul class="list_reset list_orders">
                                    <li>
                                        <?= Loc::getMessage('SPOL_TPL_SUM_TO_PAID') ?><span
                                                class="font_normal"><?= $payment['FORMATED_SUM'] ?></span>
                                    </li>
                                    <?
                                    if ($payment['PAID'] === 'Y') {
                                        ?>
                                        <li>Статус: <span
                                                    class="font_normal"><?= strtolower(Loc::getMessage('SPOL_TPL_PAID')) ?></span>
                                        </li>
                                        <?
                                    } else {
                                        ?>
                                        <li>Статус: <span
                                                    class="font_normal"><?= strtolower(Loc::getMessage('SPOL_TPL_NOTPAID')) ?></span>
                                        </li>
                                        <?
                                    }
                                    ?>
                                    <li>Способ оплаты: <span
                                                class="font_normal"><?= strtolower($payment['PAY_SYSTEM_NAME']) ?></span>
                                    </li>
                                    <li>Состав заказа: <br/>
                                        <a href="#">1. Розы</a>
                                    </li>
                                    <div class="sale-order-list-payment-title">
                                        <?
                                        $paymentSubTitle = Loc::getMessage('SPOL_TPL_BILL') . " " . Loc::getMessage('SPOL_TPL_NUMBER_SIGN') . htmlspecialcharsbx($payment['ACCOUNT_NUMBER']);
                                        if (isset($payment['DATE_BILL'])) {
                                            $paymentSubTitle .= " " . Loc::getMessage('SPOL_TPL_FROM_DATE') . " " . $payment['DATE_BILL']->format($arParams['ACTIVE_DATE_FORMAT']);
                                        }
                                        $paymentSubTitle .= ",";
                                        echo $paymentSubTitle;
                                        ?>


                                    </div>
                                    <div class="sale-order-list-payment-price">
                            <span class="sale-order-list-payment-element"><?= Loc::getMessage('SPOL_TPL_SUM_TO_PAID') ?>
                                : </span>

                                        <span class="sale-order-list-payment-number"><?= $payment['FORMATED_SUM'] ?></span>
                                    </div>
                                </ul>
                            </td>
                            <td>
                                <ul class="list_reset list_orders">
                                    <li><?= $payment['DATE_BILL']->format($arParams['ACTIVE_DATE_FORMAT']) ?></li>
                                    <li>
                                        <span class="label_refresh label-warning"><?= htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME']) ?></span>
                                    </li>
                                    <li><a class=""
                                           href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"]) ?>"><?= Loc::getMessage('SPOL_TPL_CANCEL_ORDER') ?></a>
                                    </li>
                                    <li><a class=""
                                           href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"]) ?>"><?= Loc::getMessage('SPOL_TPL_REPEAT_ORDER') ?></a>
                                    </li>

                                </ul>
                            </td>


                            <?
                        }

                        ?>


                    </tr>
                </table>
            </div>
            <?}?>
            </div>
            </div>

            <?
    }

    ?>

    <?

}
?>
