<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
    ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0):
    ?>
    <div role="tabpanel" class="tab-pane active" id="basket">
        <div class="bx_ordercart_order_table_container table-responsive">
            <table class="vertical_align" id="basket_items">
                <tr>
                    <?
                    foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
                        $arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
                        if ($arHeader["name"] == '')
                            $arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
                        $arHeaders[] = $arHeader["id"];

                        // remember which values should be shown not in the separate columns, but inside other columns
                        if (in_array($arHeader["id"], array("TYPE")))
                        {
                            $bPriceType = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "PROPS")
                        {
                            $bPropsColumn = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "DELAY")
                        {
                            $bDelayColumn = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "DELETE")
                        {
                            $bDeleteColumn = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "WEIGHT")
                        {
                            $bWeightColumn = true;
                        }

                        if ($arHeader["id"] == "NAME"):
                            ?>
                            <th><?
                        elseif ($arHeader["id"] == "PRICE"):
                            ?>
                            <th><?
                        else:
                            ?><th><?
                        endif;
                        ?>
                        <?=$arHeader["name"]; ?>
                        </th>
                        <?
                    endforeach;
                    ?>

                    <th></th>
                </tr>
                <?
                foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
                    if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
                        ?>
                        <tr id="<?=$arItem["ID"]?>">
                            <?
                            foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

                                if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in the columns in this template
                                    continue;

                                if ($arHeader["name"] == '')
                                    $arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);

                                if ($arHeader["id"] == "NAME"):
                                    ?>
                                    <td>
                                        <div class="basket_table_picture">
                                            <?
                                            if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
                                                $url = $arItem["PREVIEW_PICTURE_SRC"];
                                            elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
                                                $url = $arItem["DETAIL_PICTURE_SRC"];
                                            else:
                                                $url = $templateFolder."/images/no_photo.png";
                                            endif;
                                            ?>

                                            <img src="<?= $url ?>" alt="img"/>
                                        </div>
                                        <?
                                        if (!empty($arItem["BRAND"])):
                                            ?>
                                            <div class="bx_ordercart_brand">
                                                <img alt="" src="<?=$arItem["BRAND"]?>" />
                                            </div>
                                            <?
                                        endif;
                                        ?>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "PROPERTY_DESCRIPTION_VALUE"):
                                    ?>
                                    <td>
                                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"> <strong><?=$arItem["NAME"]; ?></strong></a>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "QUANTITY"):
                                    $ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
                                    $max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
                                    $useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
                                    $useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
                                    ?>
                                    <?
                                    if (!isset($arItem["MEASURE_RATIO"]))
                                    {
                                        $arItem["MEASURE_RATIO"] = 1;
                                    } if (
                                    floatval($arItem["MEASURE_RATIO"]) != 0
                                ):
                                    ?><td>
                                    <div class="counter _min">
                                        <a href="javascript:void(0);" class="counter__minus minus"
                                           onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'down', <?= $useFloatQuantityJS ?>);">-</a>
                                        <input class="counter__inp"
                                               type="text"
                                               size="3"
                                               id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
                                               name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
                                               size="2"
                                               maxlength="18"
                                               min="0"
                                            <?=$max?>
                                               step="<?=$ratio?>"
                                               value="<?=$arItem["QUANTITY"]?>"
                                               onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
                                        >
                                        <a href="javascript:void(0);" class="counter__plus"
                                           onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'up', <?= $useFloatQuantityJS ?>);">+</a>
                                    </div>
                                    </td>
                                    <?
                                endif;
                                    ?>
                                    <input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
                                    <?
                                elseif ($arHeader["id"] == "PRICE"):
                                    ?>
                                    <td>
                                        <?= $arItem["PRICE_FORMATED"] ?>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "DISCOUNT"):
                                    ?>
                                    <td class="custom">
                                        <span><?=$arHeader["name"]; ?>:</span>
                                        <div id="discount_value_<?=$arItem["ID"]?>"><?=$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"]?></div>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "WEIGHT"):
                                    ?>
                                    <td class="custom">
                                        <span><?=$arHeader["name"]; ?>:</span>
                                        <?=$arItem["WEIGHT_FORMATED"]?>
                                    </td>
                                    <?
                                else:
                                    ?>
                                    <td>

                                        <span><?= $arHeader["name"]; ?>:</span>
                                        <?
                                        if ($arHeader["id"] == "SUM"):
                                        ?>
                                        <div id="sum_<?= $arItem["ID"] ?>">
                                            <?
                                            endif;

                                            echo $arItem[$arHeader["id"]];

                                            if ($arHeader["id"] == "SUM"):
                                            ?>
                                        </div>
                                    <?
                                    endif;
                                    ?>
                                    </td>
                                    <?
                                endif;
                            endforeach;

                            if ($bDelayColumn || $bDeleteColumn):
                                ?>
                                <td>
                                    <?
                                    if ($bDeleteColumn):
                                        ?>
                                        <a class="fz_22" title="Удалить"
                                           href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>"><i
                                                    class="icon icon-trash"></i></a>
                                        <?
                                    endif;
                                    if ($bDelayColumn):
                                        ?>
                                        <a class="fz_22" title="Отложить"
                                           href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delay"]) ?>"><i
                                                    class="icon icon-bag"></i></a>
                                        <?
                                    endif;
                                    ?>
                                </td>
                                <?
                            endif;
                            ?>
                        </tr>
                        <?
                    endif;
                endforeach;
                ?>

            </table>
        </div>
        <input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
        <input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
        <input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
        <input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
        <input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
        <div class="row">
            <div class="col-xs-12 ">
                <div class="fl_right">
                    <dl class="dl-horizontal">
                        <? if ($bWeightColumn && floatval($arResult['allWeight']) > 0): ?>
                            <tr>
                                <td class="custom_t1"><?= GetMessage("SALE_TOTAL_WEIGHT") ?></td>
                                <td class="custom_t2" id="allWeight_FORMATED"><?= $arResult["allWeight_FORMATED"] ?>
                                </td>
                            </tr>
                        <? endif; ?>
                        <? if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"): ?>
                            <tr>
                                <td><? echo GetMessage('SALE_VAT_EXCLUDED') ?></td>
                                <td id="allSum_wVAT_FORMATED"><?= $arResult["allSum_wVAT_FORMATED"] ?></td>
                            </tr>
                            <? if (floatval($arResult["DISCOUNT_PRICE_ALL"]) > 0): ?>
                                <tr>
                                    <td class="custom_t1"></td>
                                    <td class="custom_t2" style="text-decoration:line-through; color:#828282;"
                                        id="PRICE_WITHOUT_DISCOUNT">
                                        <?= $arResult["PRICE_WITHOUT_DISCOUNT"] ?>
                                    </td>
                                </tr>
                            <? endif; ?>
                            <?
                            if (floatval($arResult['allVATSum']) > 0):
                                ?>
                                <tr>
                                    <td><? echo GetMessage('SALE_VAT') ?></td>
                                    <td id="allVATSum_FORMATED"><?= $arResult["allVATSum_FORMATED"] ?></td>
                                </tr>
                                <?
                            endif;
                            ?>
                        <? endif; ?>
                        <dt>Товаров :</dt>
                        <dd><?= count($arResult["GRID"]["ROWS"]) ?></dd>
                        <dt class=""><?= GetMessage("SALE_TOTAL") ?></dt>
                        <dd id="allSum_FORMATED"
                            class="font_bold fz_18"><?= str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"]) ?></dd>

                    </dl>
                </div>

            </div>
        </div>
        <div class="col-xs-12 mt_2">
            <div class="text_center">
                <a href="/personal/order/make/" class="btn btn-primary btn-lg js_next checkout">Далее</a>
            </div>
        </div>
    </div>

    <?
else:
    ?>
    <div id="basket_items_list">
        <table>
            <tbody>
            <tr>
                <td style="text-align:center">
                    <div class=""><?= GetMessage("SALE_NO_ITEMS"); ?></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <?
endif;
?>