<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(empty($arResult["ITEMS"]["DelDelCanBuy"])) {
    ?><p class="message_not">В избранном ещё нет товаров.</p><?
}
else {
    ?>
    <b><?= GetMessage("SALE_OTLOG_TITLE") ?></b>
    <div class="table-responsive">
        <table class="vertical_align">
            <?
            foreach ($arResult["ITEMS"]["DelDelCanBuy"] as $arBasketItems) {
                ?>
                <tr>
                    <? if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
                        <td>
                            <div class="basket_table_picture">
                                <?
                                if (strlen($arBasketItems["PREVIEW_PICTURE_SRC"]) > 0):
                                    $url = $arBasketItems["PREVIEW_PICTURE_SRC"];
                                elseif (strlen($arBasketItems["DETAIL_PICTURE_SRC"]) > 0):
                                    $url = $arBasketItems["DETAIL_PICTURE_SRC"];
                                else:
                                    $url = $templateFolder . "/images/no_photo.png";
                                endif;
                                ?>
                                <img src="<?= $url ?>" alt="img"/>
                            </div>
                            <?
                            if (!empty($arBasketItems["BRAND"])):
                                ?>
                                <div class="basket_table_picture">
                                    <img alt="img" src="<?= $arBasketItems["BRAND"] ?>"/>
                                </div>
                                <?
                            endif;
                            ?>
                        </td>
                    <?endif; ?>
                    <? if (isset($arBasketItems["NAME"])):?>
                        <td>
                            <a class="mt_1 i_block" href="<?= $arBasketItems["DETAIL_PAGE_URL"]?>">
                                <strong><?= $arBasketItems["NAME"] ?></strong>
                            </a>
                        </td>
                    <?endif; ?>
                    <? if (isset($arBasketItems["PROPERTY_DESCRIPTION_VALUE"])):?>
                        <td>
                            <a class="mt_1 i_block" href="#">
                                <strong><?= $arBasketItems["PROPERTY_DESCRIPTION_VALUE"] ?></strong></a> <br/>
                        </td>
                    <?endif; ?>
                    <? if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
                        <td class="text_center">
                            <?= $arBasketItems["PRICE_FORMATED"] ?>
                        </td>
                    <?endif; ?>
                    <? if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
                        <td><? echo $arBasketItems["NOTES"] ?></td>
                    <?endif; ?>
                    <? if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
                    <?endif; ?>
                    <? if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
                        <td>
                            <a class="fz_22" title="Удалить"
                               href="<?= str_replace("#ID#", $arBasketItems["ID"], $arUrls["delete"]) ?>"><i
                                    class="icon icon-trash"></i></a>
                            <? if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>

                                <a class="fz_22" title="В корзину"
                                   href="<?= str_replace("#ID#", $arBasketItems["ID"], $arUrls["add"]) ?>"><i
                                        class="icon icon-basket"></i></a>

                            <?endif; ?>
                        </td>
                    <?endif; ?>
                    <? if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
                        <td align="right"><? echo $arBasketItems["WEIGHT_FORMATED"] ?></td>
                    <?endif; ?>
                </tr>
                <?
            }
            ?>
        </table>
    </div>
    <?
}