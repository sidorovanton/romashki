<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
    ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

if ($normalCount > 0):
    ?>


        <div class="table-responsive">
            <table class="vertical_align">
                <?
                foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
                    $arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
                    if ($arHeader["name"] == '')
                        $arHeader["name"] = GetMessage("SALE_" . $arHeader["id"]);
                    $arHeaders[] = $arHeader["id"];

                    // remember which values should be shown not in the separate columns, but inside other columns
                    if (in_array($arHeader["id"], array("TYPE"))) {
                        $bPriceType = true;
                        continue;
                    } elseif ($arHeader["id"] == "PROPS") {
                        $bPropsColumn = true;
                        continue;
                    } elseif ($arHeader["id"] == "DELAY") {
                        $bDelayColumn = true;
                        continue;
                    } elseif ($arHeader["id"] == "DELETE") {
                        $bDeleteColumn = true;
                        continue;
                    } elseif ($arHeader["id"] == "WEIGHT") {
                        $bWeightColumn = true;
                    }
                endforeach;
                ?>

                <?

                foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

                    if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
                        ?>
                        <tr>
                            <?
                            foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

                                if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in the columns in this template
                                    continue;

                                if ($arHeader["name"] == '')
                                    $arHeader["name"] = GetMessage("SALE_" . $arHeader["id"]);

                                if ($arHeader["id"] == "NAME"):
                                    ?>
                                    <td>
                                        <div class="basket_table_picture">
                                            <?
                                            if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
                                                $url = $arItem["PREVIEW_PICTURE_SRC"];
                                            elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
                                                $url = $arItem["DETAIL_PICTURE_SRC"];
                                            else:
                                                $url = $templateFolder . "/images/no_photo.png";
                                            endif;
                                            ?>

                                            <img src="<?= $url ?>" alt="img"/>


                                        </div>
                                        <?
                                        if (!empty($arItem["BRAND"])):
                                            ?>
                                            <div class="basket_table_picture">
                                                <img alt="img" src="<?= $arItem["BRAND"] ?>"/>
                                            </div>
                                            <?
                                        endif;
                                        ?>
                                    </td>

                                    <?
                                elseif ($arHeader["id"] == "QUANTITY"):

                                elseif ($arHeader["id"] == "PRICE"):
                                    ?>
                                    <td>
                                        <?= $arItem["PRICE_FORMATED"] ?>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "DISCOUNT"):
                                    ?>
                                    <td class="custom">
                                        <span><?= $arHeader["name"]; ?>:</span>
                                        <div id="discount_value_<?= $arItem["ID"] ?>"><?= $arItem["DISCOUNT_PRICE_PERCENT_FORMATED"] ?></div>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "PROPERTY_DESCRIPTION_VALUE"):
                                    ?>
                                    <td>
                                        <a class="mt_1 i_block" href="#">
                                            <strong><?= $arItem[$arHeader["id"]] ?></strong></a> <br/>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "WEIGHT"):
                                    ?>
                                    <td class="custom">
                                        <span><?= $arHeader["name"]; ?>:</span>
                                        <?= $arItem["WEIGHT_FORMATED"] ?>
                                    </td>
                                    <?
                                else:

                                    ?>
                                    <td>

                                        <span><?= $arHeader["name"]; ?>:</span>
                                        <?
                                        if ($arHeader["id"] == "SUM"):
                                        ?>
                                        <div id="sum_<?= $arItem["ID"] ?>">
                                            <?
                                            endif;

                                            echo $arItem[$arHeader["id"]];

                                            if ($arHeader["id"] == "SUM"):
                                            ?>
                                        </div>
                                    <?
                                    endif;
                                    ?>
                                    </td>
                                    <?
                                endif;
                            endforeach;

                            if ($bDelayColumn || $bDeleteColumn):
                                ?>
                                <td>
                                    <?
                                    if ($bDeleteColumn):
                                        ?>
                                        <a class="fz_22" title="Удалить"
                                           href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>"><i
                                                    class="icon icon-trash"></i></a><br/>
                                        <?
                                    endif;
                                    if ($bDelayColumn):
                                        ?>
                                        <a class="fz_22" title="Отложить"
                                           href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delay"]) ?>"><i
                                                    class="icon icon-bag"></i></a>
                                        <?
                                    endif;
                                    ?>
                                </td>
                                <?
                            endif;
                            ?>
                        </tr>
                        <?
                    endif;
                endforeach;
                ?>

            </table>
        </div>

        <input type="hidden" id="column_headers" value="<?= CUtil::JSEscape(implode($arHeaders, ",")) ?>"/>
        <input type="hidden" id="offers_props"
               value="<?= CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ",")) ?>"/>
        <input type="hidden" id="action_var" value="<?= CUtil::JSEscape($arParams["ACTION_VARIABLE"]) ?>"/>
        <input type="hidden" id="quantity_float" value="<?= $arParams["QUANTITY_FLOAT"] ?>"/>
        <input type="hidden" id="count_discount_4_all_quantity"
               value="<?= ($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N" ?>"/>
        <input type="hidden" id="price_vat_show_value"
               value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>"/>
        <input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>"/>
        <input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>"/>
        <input type="hidden" id="auto_calculation"
               value="<?= ($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y" ?>"/>

    <?
else:
    ?>
    <div id="basket_items_list">
        <table>
            <tbody>
            <tr>
                <td style="text-align:center">
                    <div class=""><?= GetMessage("SALE_NO_ITEMS"); ?></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <?
endif;
?>