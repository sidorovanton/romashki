<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
		<footer class="g-footer container-fluid footer_text footer__bg">
            <div class="container">
                <div class="row">
                    <div class="row__inline-blocks text_center__mid-xs">
                        <div class="col-xs-12 col-mid-xs-6 col-sm-3">
                            <div class="footer_title">
                                <? $APPLICATION->IncludeComponent(
                                	"bitrix:main.include",
									"",
									array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."local/include/about_title.php"
									),
									false
								);?>
                            </div>
                            <div class="footer_menu list_reset">
								<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
									"ROOT_MENU_TYPE" => "bottom",
									"MAX_LEVEL" => "1",
									"MENU_CACHE_TYPE" => "A",
									"CACHE_SELECTED_ITEMS" => "N",
									"MENU_CACHE_TIME" => "36000000",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(
									),
								),
									false
								);?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-mid-xs-6 col-sm-3">
                            <div class="footer_title">
								<? $APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."local/include/contacts_title.php"),
									false
								);?>
							</div>
                            <div>
								<? $APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."local/include/contacts_address.php"),
									false
								);?>
							</div>
                            <div class="Lora-BoldItalic phone_footer">
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "inc",
										"EDIT_TEMPLATE" => "",
										"PATH" => SITE_DIR."/local/include/number_phone1.php"
									)
								);?>
							</div>
							<div class="Lora-BoldItalic phone_footer">
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "inc",
										"EDIT_TEMPLATE" => "",
										"PATH" => SITE_DIR."/local/include/number_phone1.php"
									)
								);?>
							</div>
                            <div class="fz_16 mt_1">
								<? $APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."local/include/contacts_schedule.php"),
									false
								);?>
							</div>
                        </div>
                        <div class="col-xs-12 col-mid-xs-6 col-sm-3">
                            <? $APPLICATION->IncludeComponent(
                            	"bitrix:main.include",
								"",
								array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."/local/include/schedule.php"
								),
								false
							); ?>
                        </div>
                        <div class="col-xs-12 col-mid-xs-6 col-sm-3 text_center">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."local/include/socnet_sidebar.php",
									"EDIT_MODE" => "html",
								),
								false
							);?>
                        </div>
                        <div class="clear"></div>
                        <hr class="hr_gray">
                        <div class="col-xs-12">
                            <div class="fl_left">
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR."local/include/about_site.php",
										"EDIT_MODE" => "html",
									),
									false
								);?>
                            </div>
                            <div class="fl_right ">Разработка сайта - <a target="_blank" href="imedia.by"><img src="<?=SITE_TEMPLATE_PATH?>/images/im-logo.png" alt="img"></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
		<div id="popup-enter" data-effect="mfp-zoom-in" class="mfp-with-anim white-popup mfp-hide clearfix">
			<div class="popup_header">
				<div class="title_line_horizontal title_line_decor">
					<span>Войти</span>
				</div>
			</div>
			<div class="col-xs-offset-1 col-xs-10 popup_wrap">
				<form class="clearfix" action="">
					<div class="form-group">
						<label for="login">Логин</label>
						<input type="text" class="form-control" id="login" placeholder="">
					</div>
					<div class="form-group">
						<label for="pass">Пароль</label>
						<input type="text" class="form-control" id="pass" placeholder="">
					</div>

					<input id="check" type="checkbox"/>
					<label class="font_normal" for="check">Запомнить меня на этом компьютере</label>

					<div class= "col-xs-offset-2 col-xs-8 text_center mt_2 mb_2">
						<input class="btn btn-primary full_width _upper" value="Войти" type="submit"/>
					</div>
				</form>
				<div class="text-center">
					<a class="fz_12 open-popup-inline" href="#popup-rduction">Забыли пароль?</a>
					<a class=" fz_12" href="#">Зарегистрироваться</a>
				</div>
			</div>
		</div>
		<div id="popup-registration" data-effect="mfp-zoom-in" class=" white-popup mfp-with-anim mfp-hide clearfix">
			<div class="popup_header">
				<div class="title_line_horizontal title_line_decor">
					<span>Регистрация</span>
				</div>
			</div>
			<div class="col-xs-offset-1 col-xs-10 popup_wrap">
				<form class="clearfix" action="">
					<div class="form-group">
						<label for="name">Имя</label>
						<input type="text" class="form-control" id="name" placeholder="">
					</div>
					<div class="form-group">
						<label for="fam">Фамилия</label>
						<input type="text" class="form-control" id="fam" placeholder="">
					</div>
					<div class="form-group has-error">
						<label for="login"> Логин (минимум 3 символа)<span class="color_danger">*</span></label>
						<input type="text" class="form-control" id="login" placeholder="">
					</div>

					<div class="form-group has-error">
						<label for="pass"> Пароль<span class="color_danger">*</span></label>
						<input type="text" class="form-control" id="pass" placeholder="">
					</div>

					<div class="form-group">
						<label for="passpod">Подтверждение пароля<span class="color_danger">*</span></label>
						<input type="text" class="form-control" id="passpod" placeholder="">
					</div>

					<div class="form-group">
						<label for="mail">Ваша почта<span class="color_danger">*</span></label>
						<input type="text" class="form-control" id="mail" placeholder="">
					</div>

					<div class="form-group">
						<label for="captcha">Введите символы на картинке<span class="color_danger">*</span></label>
						<div class="bx-captcha"><img src="" width="180" height="40" alt="CAPTCHA"></div>
						<input type="text" class="form-control" id="captcha" placeholder="">
					</div>

					<div class= "col-xs-offset-2 col-xs-8 text_center mt_2 mb_1">
						<input class="btn btn-primary full_width _upper" value="Регистрация" type="submit"/>
					</div>

				</form>
				<hr/>
				<div class="text-center fz_12">
					Пароль не должен быть менее 6 символов. <br/>
					<span class="color_danger">*</span> Поля, обязательные для заполнения.<br/>
					<a class="" href="#">Авторизация</a>
				</div>

			</div>

		</div>
		<div id="popup-rduction" data-effect="mfp-zoom-in" class="mfp-with-anim white-popup mfp-hide clearfix">
			<div class="popup_header">
				<div class="title_line_horizontal title_line_decor">
					<span>Восстановить пароль</span>
				</div>
			</div>
			<div class="col-xs-offset-1 col-xs-10 popup_wrap">
				<form class="clearfix" action="">
					<div class="form-group">
						<label for="login">Введите Ваше имя</label>
						<input type="text" class="form-control" id="login" placeholder="">
					</div>
					<div class="form-group">
						<label for="pass">Введите Ваш e-mail</label>
						<input type="text" class="form-control" id="pass" placeholder="">
					</div>

					<div class= "col-xs-offset-2 col-xs-8 text_center mt_2 mb_2">
						<input class="btn btn-primary full_width _upper" value="Отправить" type="submit"/>
					</div>
				</form>
			</div>
		</div>
		<div id="card_in_basket" data-effect="mfp-zoom-in" class="mfp-with-anim white-popup mfp-hide clearfix open-custom-cart">
			<div class="popup_header">
				<div class="title_line_horizontal title_line_decor"></div>
			</div>
			<div class="col-xs-offset-1 col-xs-10 popup_wrap text_center">
				<div class="text_center custom_content"></div>
				<a href="javascript:void(0)" class="btn btn-primary close-button">Продолжить покупки</a>
				<a href="#" id="link_show" class="btn btn-primary redirect-cart">Оформить заказ</a>
			</div>
		</div>
		<div id="popup-click" data-effect="mfp-zoom-in" class="mfp-with-anim white-popup mfp-hide clearfix">
			<div class="popup_header">
				<div class="title_line_horizontal title_line_decor">
					<span>ЗАПОЛНИТЕ ФОРМУ БЫСТРОГО ЗАКАЗА</span>
				</div>
			</div>
			<div class="col-xs-offset-1 col-xs-10 popup_wrap">
				<div class="text_center custom_text_pop"></div>
				<form class="clearfix" id="feed_order_user" method="post" name="send_order_popap">
					<div class="form-group col-xs-12 col-sm-6">
						<label for="login">Ваше имя</label>
						<input type="text" name="user_name" class="form-control" id="login" placeholder="Ваше имя" required="">
					</div>
					<div class="form-group col-xs-12 col-sm-6">
						<label for="pass">Ваш телефон</label>
						<input id="phone" name="phone" pattern="^\+375(\s+)?\(?(17|25|29|33|44)\)?(\s+)?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$" type="text" name="user_phone" class="form-control" id="pass" placeholder="Ваш телефон" required="">
					</div>
					<div class="form-group col-xs-12">
						<label for="addr">Адрес доставки</label>
						<input type="text" name="user_addres" class="form-control" id="addr" placeholder="Адрес доставки" required="">
					</div>
					<input type="hidden" name="nIdProd" value="">
					<input type="hidden" name="nPriceProd" value="">
					<input type="hidden" name="sCurrens" value="RUB">
					<input type="hidden" name="sSiteID" value="s1">
					<input type="hidden" name="sProdName" value="">
					<input type="hidden" name="nQuentityProd" value="">
					<input type="hidden" name="send_ajax_feed" value="Y">
					<div class="col-xs-offset-2 col-xs-8 text_center mt_2 mb_2">
						<input class="btn btn-primary full_width" value="Отправить заказ" type="submit">
					</div>
				</form>
			</div>
		</div>
		<div id="add_order_one_click" style="display:none;"></div>
		<div id="in_wish" data-effect="mfp-zoom-in" class="mfp-with-anim white-popup mfp-hide clearfix">
			<div class="popup_header">
				<div class="title_line_horizontal title_line_decor">
					<span>ТОВАР ДОБАВЛЕН В ИЗБРАННОЕ</span>
				</div>
			</div>
			<div class="col-xs-offset-1 col-xs-10 popup_wrap text_center">
				<div class="text_center custom_popap"></div>
				<a href="javascript:void(0)" onclick="$.magnificPopup.close();" class="btn btn-primary">Продолжить покупки</a>
				<a href="/wish/" id="link_show" class="btn btn-primary">Перейти в избранное</a>
			</div>
		</div>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery.2.1.4.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/tab.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/collapse.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/collapse.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/menu_collapse.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/dropdown.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/pushy.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/transition.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery.magnific-popup.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/owl.carousel.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/custom.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery.maskedinput.js"></script>

<script>
    $('div[id^=bx_incl_area]').css('display','inline');
</script>
</body>
</html>