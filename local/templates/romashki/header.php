<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
$theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);
?>
<!DOCTYPE html>
<html xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
    <link rel="icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico" type="image/x-icon">
    <meta name="description" content=" ">
    <meta name="Keywords" content="">
    <meta name="robots" content="all">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/app.min.css">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <? $APPLICATION->ShowHead(); ?>
    <? $APPLICATION->ShowPanel() ?>
    <title><? $APPLICATION->ShowTitle() ?></title>
</head>
<body class="main" id="_pr">

<script>
    document.getElementsByTagName('body')[0].classList.add('load_opacity');
    document.addEventListener('DOMContentLoaded', function () {
        document.getElementsByTagName('body')[0].classList.remove('load_opacity');
    });
</script>
<header class="container-fluid header_3">
    <div class="header_content">
        <div class="header_top">
            <div class="container">
                <div class="row">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "top_menu",
                        Array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "COMPONENT_TEMPLATE" => ".default",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => "",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "top",
                            "USE_EXT" => "N"
                        )
                    ); ?>
                    <div class="col-xs-12 col-mid-xs-12 col-sm-4 col-md-4 header_phone text-center">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/local/include/number_phone1.php"
                            )
                        ); ?>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/local/include/number_phone2.php"
                            )
                        ); ?>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-mid-xs-12 col-md-3 text-right mid-xs_text_center">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:system.auth.form",
                                "auth",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "FORGOT_PASSWORD_URL" => "",
                                    "PROFILE_URL" => "/personal/cart/",
                                    "REGISTER_URL" => "",
                                    "SHOW_ERRORS" => "Y"
                                )
                            ); ?>

                            <? $APPLICATION->IncludeComponent("bitrix:main.register", "register", Array(
                                "AUTH" => "Y",    // Автоматически авторизовать пользователей
                                "REQUIRED_FIELDS" => array(    // Поля, обязательные для заполнения
                                    0 => "EMAIL",
                                ),
                                "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
                                "SHOW_FIELDS" => array(    // Поля, которые показывать в форме
                                    0 => "EMAIL",
                                    1 => "NAME",
                                    2 => "LAST_NAME",
                                ),
                                "SUCCESS_PAGE" => "/",    // Страница окончания регистрации
                                "USER_PROPERTY" => "",    // Показывать доп. свойства
                                "USER_PROPERTY_NAME" => "",    // Название блока пользовательских свойств
                                "USE_BACKURL" => "Y",    // Отправлять пользователя по обратной ссылке, если она есть
                            ),
                                false
                            ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="row__inline-blocks">
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/logo.php"
                                )
                            ); ?>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <? $APPLICATION->IncludeComponent("bitrix:search.form", "search.form", Array(
                                "PAGE" => "#SITE_DIR#search/index.php",    // Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
                                "USE_SUGGEST" => "N",    // Показывать подсказку с поисковыми фразами
                            ),
                                false
                            ); ?>
                        </div>
                        <div class="col-xs-6 col-mid-xs-6 col-sm-6 col-md-2 text_right text_center__sm">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:sale.basket.basket",
                                "wish_list",
                                Array(
                                    "ACTION_VARIABLE" => "basketAction",
                                    "AUTO_CALCULATION" => "Y",
                                    "COLUMNS_LIST" => array(0=>"NAME",1=>"DISCOUNT",2=>"WEIGHT",3=>"DELETE",4=>"DELAY",5=>"TYPE",6=>"PRICE",7=>"QUANTITY",),
                                    "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                                    "GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
                                    "GIFTS_CONVERT_CURRENCY" => "N",
                                    "GIFTS_HIDE_BLOCK_TITLE" => "N",
                                    "GIFTS_HIDE_NOT_AVAILABLE" => "N",
                                    "GIFTS_MESS_BTN_BUY" => "Выбрать",
                                    "GIFTS_MESS_BTN_DETAIL" => "Подробнее",
                                    "GIFTS_PAGE_ELEMENT_COUNT" => "4",
                                    "GIFTS_PLACE" => "BOTTOM",
                                    "GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
                                    "GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
                                    "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
                                    "GIFTS_SHOW_IMAGE" => "Y",
                                    "GIFTS_SHOW_NAME" => "Y",
                                    "GIFTS_SHOW_OLD_PRICE" => "N",
                                    "GIFTS_TEXT_LABEL_GIFT" => "Подарок",
                                    "HIDE_COUPON" => "N",
                                    "OFFERS_PROPS" => "",
                                    "PATH_TO_ORDER" => "/personal/order.php",
                                    "PRICE_VAT_SHOW_VALUE" => "N",
                                    "QUANTITY_FLOAT" => "N",
                                    "SET_TITLE" => "Y",
                                    "TEMPLATE_THEME" => "blue",
                                    "USE_GIFTS" => "Y",
                                    "USE_PREPAYMENT" => "N"
                                )
                            );?>
                        </div>
                        <div class="col-xs-6 col-mid-xs-6 text_center__sm  col-sm-6 col-md-3">
                            <? $APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line", 
	"link_basket", 
	array(
		"HIDE_ON_BASKET_PAGES" => "N",
		"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
		"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"PATH_TO_PROFILE" => SITE_DIR."personal/",
		"PATH_TO_REGISTER" => SITE_DIR."login/",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "N",
		"SHOW_EMPTY_VALUES" => "Y",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_PERSONAL_LINK" => "Y",
		"SHOW_PRODUCTS" => "N",
		"SHOW_TOTAL_PRICE" => "Y",
		"COMPONENT_TEMPLATE" => "link_basket"
	),
	false
); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="bg_menu container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <nav class="menu_table pushy pushy-left">
                            <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"main_menu", 
	array(
		"ROOT_MENU_TYPE" => "left",
		"MAX_LEVEL" => "3",
		"CHILD_MENU_TYPE" => "podmenu",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "main_menu"
	),
	false
);?>
                        </nav>
                        <div class="site-overlay"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>