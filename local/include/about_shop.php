<div class="container-fluid bg_about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 main_about">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <img alt="img" src="/local/templates/romashki/images/about.jpg" class="img-responsive img_about">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <div class=" h3 mt_0">
                            О магазине цветов Romashki.by
                        </div>
                        <p>Заказывайте доставку цветов в Молодечно. На нашем сайте Вы можете заказать цветы с доставкой в Молодечно. В каталоге Вы найдете букет на любой вкус и случай жизни. И не важно какое сейчас время года. Ведь любовь, ласка и доброта это те чувства, которые всегда помогают преодолеть любые преграды, будь то длительная командировка или разлука. Мы занимаем лидирующую позицию в доставке цветов по Молодечно и уже более 30 довольных получателей радуются нашим</p>
                        <a class="_underline" href="/about/">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><br>