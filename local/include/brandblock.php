<div class="b-information__item b-information__item--bdr">
    <div class="Lora-BoldItalic fz_16 mb_1 lh_1"><i class="icon icon-bus"></i>Доставка:</div>
    <ul class="list_reset b-information__item__list">
        <li>Доставим курьером</li>
        <li>Самовывоз</li>
    </ul>
</div>
<div class="b-information__item b-information__item--bdr">
    <div class="Lora-BoldItalic fz_16 mb_1 lh_1"><i class="icon icon-dollar"></i>Оплата:</div>
    <ul class="list_reset">
        <li>Пластиковой картой</li>
        <li>Наличными</li>
    </ul>
</div>
<div class="b-information__item">
    <div class="Lora-BoldItalic fz_16 mb_1 lh_1"><i class="icon icon-clock"></i>Время работы и контакты:</div>
    <ul class="list_reset">
        <li class="">9:00 - 21:00</li>
        <li class="">г.Молодечно, ул. Буйло 3</li>
        <li class="">г.Молодечно, ул. Волынца 3</li>
        <li class="">+375 29 000 00 00</li>
        <li class="">+375 29 000 00 00</li>
    </ul>
</div>