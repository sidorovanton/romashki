<div class="container-fluid main_label_bg hidden-mid-xs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="main_label">
                    <div class="main_label__pic _1"></div>
                    <div class="main_label__text">
                        Зачем куда-то ходить? Доставим бесплатно!
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="main_label">
                    <div class="main_label__pic _2"></div>
                    <div class="main_label__text">
                        Всегда только свежие цветы!
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="main_label ">
                    <div class="main_label__pic _3"></div>
                    <div class="main_label__text">
                        Пришлем фото Вашего букета!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>