<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$userName = CUser::GetFullName();
if (!$userName)
	$userName = CUser::GetLogin();
?>
<div class="g-wrapper">
	<main>
		<div class="g-main">
			<div class="g-main_i container">
				<div class="row">
					<div class="col-xs-12">
						<script>
							<?if ($userName):?>
							BX.localStorage.set("eshop_user_name", "<?=CUtil::JSEscape($userName)?>", 604800);
							<?else:?>
							BX.localStorage.remove("eshop_user_name");
							<?endif?>

							<?if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0 && preg_match('#^/\w#', $_REQUEST["backurl"])):?>
							document.location.href = "<?=CUtil::JSEscape($_REQUEST["backurl"])?>";
							<?endif?>
						</script>

						<?
						$APPLICATION->SetTitle("Авторизация");
						?>
						<div class="text-center">
							<h3>Вы зарегистрированы и успешно авторизовались.</h3>
							<p><a href="<?=SITE_DIR?>">Вернуться на главную страницу</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>