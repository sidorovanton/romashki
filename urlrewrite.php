<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#",
		"RULE" => "alias=\$1",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/personal/order/cancel/(.+?)/#",
		"RULE" => "ID=\$1",
		"ID" => "",
		"PATH" => "/personal/order/cancel/order_cancel.php",
	),
	array(
		"CONDITION" => "#^/personal/order/detail/(.+?)/#",
		"RULE" => "ID=\$1",
		"ID" => "",
		"PATH" => "/personal/order/detail/order_detail.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/online/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/basket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/basket.php",
	),
	array(
		"CONDITION" => "#^/store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/store/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
	array(
		"CONDITION" => "#^/wish/#",
		"RULE" => "",
		"ID" => "bitrix:sale.basket.basket",
		"PATH" => "/wish.php",
	),
);

?>